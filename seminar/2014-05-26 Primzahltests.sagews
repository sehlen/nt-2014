︠1b8ad951-22d7-4cb1-a76d-f56963a7be4fi︠
%md
### Wir implementieren die Fermat-, Miller-Rabin und Lucas-Lehmer Primzahltests

Dabei führe ich absichtlich ein paar neue Befehle ein, damit ihr etwas den Überblick bekommt.
︡09be869a-7f19-4921-8f15-2788a1255476︡{"html":"<h3>Wir implementieren die Fermat-, Miller-Rabin und Lucas-Lehmer Primzahltests</h3>\n\n<p>Dabei führe ich absichtlich ein paar neue Befehle ein, damit ihr etwas den Überblick bekommt.</p>\n"}︡
︠46cfec73-6c5a-4971-8707-c95b9b7046e5︠
def fermat_single(a,n):
    if Mod(a,n)^(n-1) == 1:
        return True
    else:
        return False
︡678b1473-c0e1-4a20-94fb-17f462c57828︡
︠99b37be7-03a8-4698-8b5e-67712ae3b92a︠
def fermat_is_prime(n):
    if n <= 0:
        raise ValueError("n should be a positive integer")
    if n == 1:
        return False
    for a in xrange(1,n):
        if not fermat_single(a,n):
            return False
    return True
︡20d8da1a-ea4c-48a9-b294-fe6a9de70411︡
︠0fa0d15e-1815-4f15-8034-0a65f3d34775i︠
%md
Was ist xrange?

So findet man es heraus:
︡62d26236-41bc-4fc4-9e56-08df629547e8︡{"html":"<p>Was ist xrange?</p>\n\n<p>So findet man es heraus:</p>\n"}︡
︠60ed161f-311a-4a0b-a12d-b5ad8612731f︠
xrange?
︡fa694f6c-984f-44d5-8f5f-34d025df534f︡{"stdout":"Unable to read source filename (<module '__builtin__' (built-in)> is a built-in class)   Docstring:\n   xrange(stop) -> xrange object xrange(start, stop[, step]) -> xrange\nobject\n\nLike range(), but instead of returning a list, returns an object that\ngenerates the numbers in the range on demand.  For looping, this is\nslightly faster than range() and more memory efficient.\n"}︡{"stdout":"\n"}︡
︠bddcb1a2-19e0-4f15-b605-b1aba2c6540d︠
%time A=[p for p in xrange(1,10000) if fermat_is_prime(p)]
︡e8b33c1b-eb94-411c-8875-cf0ae19605ef︡{"stdout":"CPU time: 50.72 s, Wall time: 50.70 s"}︡{"stdout":"\n"}︡
︠955a2cb3-65eb-41d1-b862-a47b75afc3c2i︠
%md
***Test auf Korrektheit (in diesem Intervall):***
︡4055a0d5-fd85-4c45-b84f-45eb7939fc7f︡{"html":"<p><strong><em>Test auf Korrektheit (in diesem Intervall):</em></strong></p>\n"}︡
︠d0f5c0bf-8aac-4425-8498-49e2a955001c︠
[p for p in A if not is_prime(p)]
︡cdf43695-3598-4d3c-97f2-9269fa3db0a9︡{"stdout":"[]\n"}︡
︠06f09281-3c05-47f9-a787-ec5811d0df63i︠
%md
***Puh!***
︡1a889cee-66ec-458a-b010-37f3b9b9433a︡{"html":"<p><strong><em>Puh!</em></strong></p>\n"}︡
︠c6ca87e0-8337-4579-bd13-59553ed83ec3i︠
%md
### Nun zum Miller-Rabin-Test
︡7b59aa77-1fb8-4747-aa5a-61aed6a60697︡{"html":"<h3>Nun zum Miller-Rabin-Test</h3>\n"}︡
︠7694d1de-2062-43a1-895f-da4e993439ff︠
def miller_rabin(n, a = None):
    if n <= 0:
        raise ValueError("n should be a positive integer")
    if n in [2,3,5]:
        return True
    if n <= 5:
        return False
    if a is None:
        a = ZZ.random_element(2,n)
    k = valuation(n-1,2)
    m = (n-1) // (2^k) # Teilen einer ganzen Zahl wenn man weiß(!), dass sie exakter Teiler ist!

    # Hier ein Beispiel, wie man assert tatsächlich korrekt einsetzt:
    # Die folgende Zeile überprüft, ob tatsächlich 2^k*m == n-1 gilt
    assert 2^k*m == n-1, "2^k*m != n-1 for k={k}, m={m}, n={n}".format(k=k,m=m,n=n)

    b = Mod(a,n)^m
    if b in [1,-1]:
        return True
    x = pow(b,2,n)
    for r in xrange(k):
        if x == -1:
            return True
        x = pow(x,2,n)
    return False
︡d32d7703-e52e-42a8-bd8f-dbc8a94093aa︡
︠e9f282d0-e1f2-48de-a497-296751f03c91︠
%time A=[p for p in xrange(1,100000) if miller_rabin(p,2) and miller_rabin(p,3)]
print len([p for p in A if not is_prime(p)]), "falsche Positive"
︡a516b76b-81a0-4f76-a384-b53784094215︡{"stdout":"CPU time: 3.00 s, Wall time: 2.99 s"}︡{"stdout":"\n"}︡{"stdout":"0"}︡{"stdout":" falsche Positive\n"}︡
︠12ec8481-8e4a-48c9-a865-9e6bff7339fdi︠
%md
***Wow! Für $p<100.000$ haben wir gerade gezeigt, dass es die Basen 2 und 3 zusammen ausreichend sind (und viel schneller als der komplette fermat-Test.)***

De facto gilt das für $p \leq 1.373.653$.
︡4280ccf1-628b-4c2e-997b-fefd5cabc1de︡{"html":"<p><strong><em>Wow! Für $p<100.000$ haben wir gerade gezeigt, dass es die Basen 2 und 3 zusammen ausreichend sind (und viel schneller als der komplette fermat-Test.)</em></strong></p>\n\n<p>De facto gilt das für $p \\leq 1.373.653$.</p>\n"}︡
︠5d399b6d-c28c-4ef9-9161-d17c416457e4i︠
%md
Nur noch mal kurz zu `assert`:
Was oben passieren würde, wenn wir einen Fehler gemacht hätten:
︡1569d9bc-488c-4eca-9c48-259ed78bfd2b︡{"html":"<p>Nur noch mal kurz zu <code>assert</code>:\nWas oben passieren würde, wenn wir einen Fehler gemacht hätten:</p>\n"}︡
︠86ca8a0b-fabe-451a-953b-7a6cc2c72270︠
k=3
m=2
n=2^3*3
assert 2^k*m == n-1, "2^k*m != n-1 for k={k}, m={m}, n={n}".format(k=k,m=m,n=n)
︡e8780983-8d2a-44dd-b5c9-e5d8622f30c2︡{"stderr":"Error in lines 4-4\nTraceback (most recent call last):\n  File \"/projects/6931ad98-daf6-4573-a651-563956d3599c/.sagemathcloud/sage_server.py\", line 733, in execute\n    exec compile(block+'\\n', '', 'single') in namespace, locals\n  File \"\", line 1, in <module>\nAssertionError: 2^k*m != n-1 for k=3, m=2, n=24\n"}︡
︠f9362ffe-70d6-40cb-80b1-c873cac6c2d1i︠
%md
Man beachte den speziellen AssertionError und die passende Fehlermeldung.
︡ecca4811-8215-4cd0-863d-99e17a88049e︡{"html":"<p>Man beachte den speziellen AssertionError und die passende Fehlermeldung.</p>\n"}︡
︠d37b3777-55f6-44cd-b1d7-5250268669b0i︠
%md
Zurück zu unseren Tests:
Wie sieht das eigentlich beim Fermat-Test aus?
︡24400bd1-5105-4a48-aa1b-3afd3ac240e5︡{"html":"<p>Zurück zu unseren Tests:\nWie sieht das eigentlich beim Fermat-Test aus?</p>\n"}︡
︠670bd704-afdb-4b39-ad1b-5454cf6f944b︠
%time A=[p for p in xrange(1,100000) if fermat_single(2,p) and fermat_single(3,p)]
X=[p for p in A if not is_prime(p)]
print len(X), "falsche Positive:", X
︡2d632f5e-76f2-4e08-9d53-67329e79bc2a︡{"stdout":"CPU time: 1.46 s, Wall time: 1.45 s"}︡{"stdout":"\n"}︡{"stdout":"24 falsche Positive: [1, 1105, 1729, 2465, 2701, 2821, 6601, 8911, 10585, 15841, 18721, 29341, 31621, 41041, 46657, 49141, 52633, 63973, 75361, 83333, 83665, 88561, 90751, 93961]\n"}︡
︠d97d4768-8a90-49d2-8c34-82076e1a3769i︠
%md
Da reichen 2 und 3 nicht aus.
︡6c5640d4-2e09-4a9e-b1d5-b96f3a1219a9︡{"html":"<p>Da reichen 2 und 3 nicht aus.</p>\n"}︡
︠d3606246-cbe7-40e2-af65-3d10fd0c1c10i︠
%md
Wie ist es mit Fermat zur Basis $2$ und Miller-Rabin zur Basis $3$ kombiniert?
︡7eabe8d9-ba1b-420b-be7b-2fa1ebd2e5cc︡{"html":"<p>Wie ist es mit Fermat zur Basis $2$ und Miller-Rabin zur Basis $3$ kombiniert?</p>\n"}︡
︠5bad4ff1-f885-4ed8-8355-96cbf84a3609︠
%time A=[p for p in xrange(1,100000) if fermat_single(2,p) and miller_rabin(p,3)]
X=[p for p in A if not is_prime(p)]
print len(X), "falsche Positive:", X
︡cc927c1c-8d4e-42c2-8e45-e23670ed601d︡{"stdout":"CPU time: 1.60 s, Wall time: 1.60 s"}︡{"stdout":"\n"}︡{"stdout":"4 falsche Positive: [8911, 10585, 18721, 31621]\n"}︡
︠6b4d2aea-48e5-4e1a-8f37-5b5848a284d9i︠
%md
Das reicht auch nicht, aber es sind doch immernoch echt wenige falsche Positive!
Denn, wie viele Primzahlen gibt es $\leq 100000$?
︡7b48733e-7ebd-453b-a039-738b65237814︡{"html":"<p>Das reicht auch nicht, aber es sind doch immernoch echt wenige falsche Positive!\nDenn, wie viele Primzahlen gibt es $\\leq 100000$?</p>\n"}︡
︠785be2f6-9a5f-4d39-a979-b838f08e231a︠
prime_pi(100000)
︡cf95cca2-74dd-452c-8a1b-bd266f205004︡{"stdout":"9592\n"}︡
︠d4b8ad5d-0f49-499e-9ea0-5e8c4c36866d︠
RR(24/9592)
︡afcff97d-62b0-495f-bf08-0c2c932dbc67︡{"stdout":"0.00250208507089241\n"}︡
︠5a8bf66f-55c1-4dbd-b513-865408775588︠
RR(4/9592)
︡dfb67279-2187-4527-842a-02c47c684b0b︡{"stdout":"0.000417014178482068\n"}︡
︠ac862c3a-66a2-443f-bb0f-5c947eaa64e9i︠
%md
Damit haben wir im schlechtesten Fall Fermat zur Basis 2 und 3 kombiniert also 0.25%, im Fall von Fermat zur Basis 2 und Miller-Rabin zur Basis 3 kombiniert sogar nur 0,042% zu viele Primzahlen erhalten!
︡f9346199-9ddf-48ec-8aba-f06a2296cf20︡{"html":"<p>Damit haben wir im schlechtesten Fall Fermat zur Basis 2 und 3 kombiniert also 0.25%, im Fall von Fermat zur Basis 2 und Miller-Rabin zur Basis 3 kombiniert sogar nur 0,042% zu viele Primzahlen erhalten!</p>\n"}︡
︠8e71d335-8f5b-4faa-bfec-6dc8e7d6e0b9i︠
%md
Übrigens: Nur Fermat zur Basis 2 liefert...
︡b0c14b22-bc34-4e8b-8afd-b78ba189b605︡{"html":"<p>Übrigens: Nur Fermat zur Basis 2 liefert&#8230;</p>\n"}︡
︠b9b1ff9c-7361-4e60-850e-ddfb6acb6ad4︠
%time A=[p for p in xrange(1,100000) if fermat_single(2,p)]
X=[p for p in A if not is_prime(p)]
print len(X), "falsche Positive:", X
︡72870869-a79f-4afd-9941-58c33bf7baa7︡{"stdout":"CPU time: 1.37 s, Wall time: 1.37 s"}︡{"stdout":"\n"}︡{"stdout":"79 falsche Positive: [1, 341, 561, 645, 1105, 1387, 1729, 1905, 2047, 2465, 2701, 2821, 3277, 4033, 4369, 4371, 4681, 5461, 6601, 7957, 8321, 8481, 8911, 10261, 10585, 11305, 12801, 13741, 13747, 13981, 14491, 15709, 15841, 16705, 18705, 18721, 19951, 23001, 23377, 25761, 29341, 30121, 30889, 31417, 31609, 31621, 33153, 34945, 35333, 39865, 41041, 41665, 42799, 46657, 49141, 49981, 52633, 55245, 57421, 60701, 60787, 62745, 63973, 65077, 65281, 68101, 72885, 74665, 75361, 80581, 83333, 83665, 85489, 87249, 88357, 88561, 90751, 91001, 93961]\n"}︡
︠e7ad6928-7885-4d8d-9c43-2b3409be2afbi︠
%md
...doch schon ein paar mehr.
︡323ddd21-a0d4-46cf-8f8b-daf48f1e00de︡{"html":"<p>&#8230;doch schon ein paar mehr.</p>\n"}︡
︠1e3be6b1-e3ce-4264-8421-4c5d435f387fi︠
%md
Jetzt mal was zum Rumspielen mit diesen Tests:

(Doppelklicken zum Anschauen des Codes)
︡4ab5491d-988e-4602-9a13-1cd628b95c19︡{"html":"<p>Jetzt mal was zum Rumspielen mit diesen Tests:</p>\n\n<p>(Doppelklicken zum Anschauen des Codes)</p>\n"}︡
︠cfd7ede5-bc4a-4a83-915e-afe1258d81cei︠
%hide
@interact
def _(N=input_box(10000,label="Obere Schranke"), number_of_random_bases=slider(range(10), default=0, label="Anzahl von zufälligen Basen"), fixed_bases = input_box("2,3", type=list, label="Feste Basen (zusätzlich)"), test_fermat=checkbox(False,label="Fermat-Test"), test_miller_rabin=checkbox(True,label="Miller-Rabin-Test")):
    t = cputime()
    pseudoprimes=[]
    for n in xrange(5,N):
        mr_prime = True
        f_prime = True
        l = [] if number_of_random_bases == 0 else [ZZ.random_element(1,n-1) for i in range(number_of_random_bases)]
        if len(fixed_bases) > 0: l.extend(fixed_bases)
        for a in l:
            if test_miller_rabin:
                mr_prime = (mr_prime and miller_rabin(n,a))
            if test_fermat:
                f_prime = (f_prime and fermat_single(a,n))
        if f_prime and mr_prime:
            pseudoprimes.append(n)
    false_positives = len([n for n in pseudoprimes if not is_prime(n)])
    t = cputime(t)
    print "False positives: {0}, Computing Time: {1}".format(false_positives, t)
︡a84d2caf-16d9-4d25-b91a-2e326b01135a︡{"interact":{"style":"None","flicker":false,"layout":[[["N",12,null]],[["number_of_random_bases",12,null]],[["fixed_bases",12,null]],[["test_fermat",12,null]],[["test_miller_rabin",12,null]],[["",12,null]]],"id":"ea1d8a7b-212f-4777-b142-44024749061e","controls":[{"control_type":"input-box","default":10000,"label":"Obere Schranke","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"N","type":null},{"control_type":"slider","default":0,"var":"number_of_random_bases","width":null,"vals":["0","1","2","3","4","5","6","7","8","9"],"animate":true,"label":"Anzahl von zufälligen Basen","display_value":true},{"control_type":"input-box","default":"2,3","label":"Feste Basen (zusätzlich)","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"fixed_bases","type":"<type 'list'>"},{"default":false,"var":"test_fermat","readonly":false,"control_type":"checkbox","label":"Fermat-Test"},{"default":true,"var":"test_miller_rabin","readonly":false,"control_type":"checkbox","label":"Miller-Rabin-Test"}]}}︡
︠c53e9fe9-8229-41f4-a4ab-338926c6ab2d︠
def is_probably_prime(n, a=None):
    if a is None:
        a = 2
    if fermat_single(a,n) and miller_rabin(n, a+1):
        return True
    else:
        return False
︡02861ff5-8c07-45a5-9a09-8fd8945f8890︡
︠875da8cd-63c8-4fa1-8ab6-d5d774cd9850︠
a = ZZ.random_element(1e100,5e100)
s = str(a)
print s[:15]+"..."+s[-15:]
︡e9ac1678-7b1c-4c7c-acc1-ba8d2680cbc3︡{"stdout":"372953661397354...479753617018080\n"}︡
︠f563fa68-8843-4256-baaf-5586b43ad123︠
is_probably_prime(a)
is_prime(a)
︡b7089bad-1d20-4ed9-bdd6-de4b9264379a︡{"stdout":"False\n"}︡{"stdout":"False\n"}︡
︠10c30a5c-8d84-4aae-b031-0369de759401︠
%timeit is_probably_prime(a)
︡799d9aa4-5441-4923-9f24-1652c16fcea3︡{"stdout":"625 loops, best of 3: 134 µs per loop"}︡{"stdout":"\n"}︡
︠37dd9987-9f8e-476d-bda3-2e9ff4949e28i︠
%md
***Wir sehen, dass wir SEHR schnell bestimmen können, dass diese riesige Zahl nicht prim ist!***

### ACHTUNG: Bei erneutem Ausführen kann es natürlich dazu kommen, dass man auch mal eine Primzahl oder Pseudo-Primzahl erwischt!
︡9a7b8559-9295-4629-a2ec-b35da7cb036c︡{"html":"<p><strong><em>Wir sehen, dass wir SEHR schnell bestimmen können, dass diese riesige Zahl nicht prim ist!</em></strong></p>\n\n<h3>ACHTUNG: Bei erneutem Ausführen kann es natürlich dazu kommen, dass man auch mal eine Primzahl oder Pseudo-Primzahl erwischt!</h3>\n"}︡
︠7b499bce-1cc9-4f80-81d8-c731e93d5540︠
%time A=[p for p in xrange(1,100000) if is_probably_prime(p)]
︡b62c506c-9b32-402b-baae-7446a3f022ce︡{"stdout":"CPU time: 1.61 s, Wall time: 1.61 s"}︡{"stdout":"\n"}︡
︠6bb0cc5b-38bf-4b82-9343-eb6f8ef222d3︠
[p for p in A if not is_prime(p)]
︡4997c3e9-3b49-468b-ba5e-9d37b74fc256︡{"stdout":"[8911, 10585, 18721, 31621]"}︡{"stdout":"\n"}︡
︠7f52d870-f013-40e3-a3f0-2ca704515b8ei︠
%md
Sowas könnte man natürlich auch machen:
Das ist vom Prinzip her sowas, wie Sage es auch macht (nur deutlich optimierter):

1. Wende zuerst einen probabilistischen Test an, zu festen Basen, so dass man bei gegebenen Schranken weiß, dass er dort sogar deterministisch ist.
2. Wende schlussendlich einen deterministischen Test an, damit das Ergebnis auch wirklich stimmt...
︡843e77a8-5e14-4d6f-9f74-e0a0232e9865︡{"html":"<p>Sowas könnte man natürlich auch machen:\nDas ist vom Prinzip her sowas, wie Sage es auch macht (nur deutlich optimierter):</p>\n\n<ol>\n<li>Wende zuerst einen probabilistischen Test an, zu festen Basen, so dass man bei gegebenen Schranken weiß, dass er dort sogar deterministisch ist.</li>\n<li>Wende schlussendlich einen deterministischen Test an, damit das Ergebnis auch wirklich stimmt&#8230;</li>\n</ol>\n"}︡
︠b0e422a5-c325-479d-82bb-8becbed085c7︠
def my_is_prime(n):
    if miller_rabin(n,2) and miller_rabin(n,3):
        if n<100000:
            return True
        if miller_rabin(n) and fermat_is_prime(n):
            return True
    return False
︡0142b848-a9ef-4745-a18a-4ce5adb808c2︡
︠6d3e9c6b-1735-4843-b1c0-5894527de406i︠
%md
# Wir kommen nun zum Lucas-Lehmer Test
***der wirklich sehr einfach zu implementieren ist!***
︡5e9070aa-6e2e-41d4-bb80-5d95b2071bf3︡{"html":"<h1>Wir kommen nun zum Lucas-Lehmer Test</h1>\n\n<p><strong><em>der wirklich sehr einfach zu implementieren ist!</em></strong></p>\n"}︡
︠1850a8fc-acf1-4e81-8cb9-69ca3b7c50bf︠
def lucas_lehmer(p):
    s = Mod(4, 2^p - 1)
    #print s
    for i in range(3, p+1):
        s = s^2 - 2
        #print s
    return s == 0
︡83e6c138-b23c-4b92-8e06-9b35ab6ad948︡
︠ea32d044-1363-484a-b5e3-09b5f0082ab6︠
def mersenne_prime_range(n, lower=1):
    A = []
    for p in prime_range(lower, n):
        #print "Testing p = ", p
        if lucas_lehmer(p):
            s = str(2^p-1)
            if len(s) > 40:
                ss = s[:18]+"..."+s[-18:]
            else:
                ss = s
            print "Mersenne prime for p = {0} with {1} digits: {2}".format(p, len(s), ss)
            A.append(p)
    print "Found {0} Mersenne primes.".format(len(A))
    return A
︡88b33f9f-fdaf-49f0-afd4-0e8957be3acf︡
︠8dac67dd-ff3d-4457-9ef3-98a7f268cd62︠
%time mersenne_prime_range(5000)
︡59acaeeb-bf6f-42a6-ac64-99755c753c9c︡{"stdout":"Mersenne prime for p = 3 with 1 digits: 7\nMersenne prime for p = 5 with 2 digits: 31\nMersenne prime for p = 7 with 3 digits: 127\nMersenne prime for p = 13 with 4 digits: 8191\nMersenne prime for p = 17 with 6 digits: 131071\nMersenne prime for p = 19 with 6 digits: 524287\nMersenne prime for p = 31 with 10 digits: 2147483647\nMersenne prime for p = 61 with 19 digits: 2305843009213693951\nMersenne prime for p = 89 with 27 digits: 618970019642690137449562111\nMersenne prime for p = 107 with 33 digits: 162259276829213363391578010288127\nMersenne prime for p = 127 with 39 digits: 170141183460469231731687303715884105727\nMersenne prime for p = 521 with 157 digits: 686479766013060971...574028291115057151\nMersenne prime for p = 607 with 183 digits: 531137992816767098...835393219031728127"}︡{"stdout":"\nMersenne prime for p = 1279 with 386 digits: 104079321946643990...710555703168729087"}︡{"stdout":"\nMersenne prime for p = 2203 with 664 digits: 147597991521418023...419497686697771007"}︡{"stdout":"\nMersenne prime for p = 2281 with 687 digits: 446087557183758429...133172418132836351"}︡{"stdout":"\nMersenne prime for p = 3217 with 969 digits: 259117086013202627...160677362909315071"}︡{"stdout":"\nMersenne prime for p = 4253 with 1281 digits: 190797007524439073...034687815350484991"}︡{"stdout":"\nMersenne prime for p = 4423 with 1332 digits: 285542542228279613...231057902608580607"}︡{"stdout":"\nFound 19 Mersenne primes."}︡{"stdout":"\n[3, 5, 7, 13, 17, 19, 31, 61, 89, 107, 127, 521, 607, 1279, 2203, 2281, 3217, 4253, 4423]\n"}︡{"stdout":"CPU time: 69.71 s, Wall time: 69.69 s\n"}︡
︠65dec806-7edb-4a6d-b78b-2e979fe2f5c4i︠
%md
### Das sind doch schon ganz schön große Primzahlen.
### Jetzt wissen wir wirklich, warum die Mersenne-Primzahlen immer alle Rekorde schlagen, oder?
***Übrigens: Bis $5000$ geht das ja ganz schnell, aber dann kommt eine große Lücke und es dauert doch eine Weile***
︡f2daa564-e549-4066-97e7-ff514b1edc14︡{"html":"<h3>Das sind doch schon ganz schön große Primzahlen.</h3>\n\n<h3>Jetzt wissen wir wirklich, warum die Mersenne-Primzahlen immer alle Rekorde schlagen, oder?</h3>\n\n<p><strong><em>Übrigens: Bis $5000$ geht das ja ganz schnell, aber dann kommt eine große Lücke und es dauert doch eine Weile</em></strong></p>\n"}︡
︠a2d22ebb-75ca-4db1-8880-1ba5be545d60i︠
%md
***Aber wir können auch große Mersenne-Zahlen einzeln noch schnell testen***
︡164fa276-7719-4227-a9a3-830a136c5a66︡{"html":"<p><strong><em>Aber wir können auch große Mersenne-Zahlen einzeln noch schnell testen</em></strong></p>\n"}︡
︠81460252-6b7d-4d24-adc4-44a96c247736︠
%time lucas_lehmer(9941)
︡ba3e375e-48df-4612-b827-8b2f6677358e︡{"stdout":"True"}︡{"stdout":"\n"}︡{"stdout":"CPU time: 2.97 s, Wall time: 2.96 s\n"}︡
︠4cf7924d-b9d4-4e25-9a0f-c4dde18d15c1︠
%time lucas_lehmer(11213)
︡d0ba3024-eb94-4156-9307-898207dab57d︡{"stdout":"True"}︡{"stdout":"\n"}︡{"stdout":"CPU time: 4.17 s, Wall time: 4.16 s\n"}︡
︠7dcde5ec-deac-4f0c-ab18-33c60e181ca3︠
%time lucas_lehmer(19937)
︡c1670a28-3bb8-493a-a2ad-b5ebc0e7acbb︡{"stdout":"True"}︡{"stdout":"\n"}︡{"stdout":"CPU time: 11.11 s, Wall time: 11.10 s\n"}︡
︠3ff67258-6db2-4d5e-9bee-ff6ff131bae0︠
%time lucas_lehmer(23209)
︡18232f43-a528-4de0-884f-1f4810410d68︡{"stdout":"True"}︡{"stdout":"\n"}︡{"stdout":"CPU time: 16.02 s, Wall time: 16.01 s\n"}︡
︠61d57b55-e013-4358-9a56-80287e7f4062︠
%time lucas_lehmer(44497)
︡16a209d5-d09a-4b88-8613-217f6fd9521c︡{"stdout":"True"}︡{"stdout":"\n"}︡{"stdout":"CPU time: 79.70 s, Wall time: 79.65 s\n"}︡
︠eed20544-33e3-4c04-8115-3b35f201b799︠
len(str(2^44497-1))
︡9e4b21f6-8add-4bce-968c-f948acb4caaa︡{"stdout":"13395\n"}︡
︠633f7f57-d8eb-4f3b-8818-fc78b6b4d0f8i︠
%md
Wir haben also gerade eine Primzahl mit 13395 Dezimalstellen 'gefunden'.... Das war mal 1979 die größte Primzahl...
︡e9d333f3-e1a8-464a-ad19-0c32caeb7d64︡{"html":"<p>Wir haben also gerade eine Primzahl mit 13395 Dezimalstellen &#8216;gefunden&#8217;&#8230;. Das war mal 1979 die größte Primzahl&#8230;</p>\n"}︡
︠957606d0-085b-4492-b971-27275431cb07i︠
%md
In der Praxis wird der Algorithmus vor Allem zusammen mit schneller Multiplikation eingesetzt.
Siehe [Wikipedia](http://en.wikipedia.org/wiki/Sch%C3%B6nhage-Strassen_algorithm).
︡7a51144d-a0fc-4185-beee-b89a11990329︡{"html":"<p>In der Praxis wird der Algorithmus vor Allem zusammen mit schneller Multiplikation eingesetzt.\nSiehe <a href=\"http://en.wikipedia.org/wiki/Sch%C3%B6nhage-Strassen_algorithm\">Wikipedia</a>.</p>\n"}︡
︠cff284b6-1105-4893-aa52-90463858dbed︠









