︠43a8065e-51bf-4863-9348-bd361a45c88ai︠
%md
# Potenzieren modulo $n$
︡282326ed-bb25-4e52-b798-3f95b6ac3720︡{"html":"<h1>Potenzieren modulo $n$</h1>\n"}︡
︠a7484b8f-e483-4085-bcd9-39aa592622c5i︠
%md
Potenzieren modulo $n$ ist *viel* schneller als gewöhnliches Potenzieren
(wenn man es richtig implementiert).

Wie geht das?

### Wir wollen $a^m \pmod{n}$ ausrechnen.
#### Dazu gehen wir wie folgt vor:

1. Schreibe m in Binärdarstellung
   \[
     m = \sum_{i=0}^r \epsilon_i 2^i
   \]
2. Berechne die Potenzen
   \[
     a, a^2, a^4=(a^2)^2, \ldots, a^{2^r}
   \]
3. Nutze aus, dass
   \[
     a^m = a^{\sum_{i=0}^r \epsilon_i 2^i} = a^{\epsilon_0} \cdot (a^2)^{\epsilon_1} \cdots (a^{2^r})^{\epsilon_r}
   \]

︡20c8ab81-c215-46eb-8615-d308518c1299︡{"html":"<p>Potenzieren modulo $n$ ist <em>viel</em> schneller als gewöhnliches Potenzieren\n(wenn man es richtig implementiert).</p>\n\n<p>Wie geht das?</p>\n\n<h3>Wir wollen $a^m \\pmod{n}$ ausrechnen.</h3>\n\n<h4>Dazu gehen wir wie folgt vor:</h4>\n\n<ol>\n<li>Schreibe m in Binärdarstellung\n\\[\n     m = \\sum_{i=0}^r \\epsilon_i 2^i\n   \\]</li>\n<li>Berechne die Potenzen\n\\[\n     a, a^2, a^4=(a^2)^2, \\ldots, a^{2^r}\n   \\]</li>\n<li>Nutze aus, dass\n\\[\n     a^m = a^{\\sum_{i=0}^r \\epsilon_i 2^i} = a^{\\epsilon_0} \\cdot (a^2)^{\\epsilon_1} \\cdots (a^{2^r})^{\\epsilon_r}\n   \\]</li>\n</ol>\n"}︡
︠c6315d04-5acf-49ae-a0e2-7a46ae0a573e︠
m=30019911
︡b1135c58-65ac-4955-833b-480e24bd1e21︡
︠8a880300-9c4d-4968-be04-17349cbcbd69i︠
%md
Zum Vergleich: Zuerst Potenzieren, dann reduzieren $\pmod{101}$.
︡6211bbcc-bf1b-4a97-a75b-0364021316b7︡{"html":"<p>Zum Vergleich: Zuerst Potenzieren, dann reduzieren $\\pmod{101}$.</p>\n"}︡
︠c569f672-2bc1-46fb-a390-3446332a6d59︠
%timeit (11^m) % 101
︡6019ea38-06c3-4c23-a84e-043a3577b3ef︡{"stdout":"5 loops, best of 3: 1.73 s per loop"}︡{"stdout":"\n"}︡
︠9996dac8-925e-4c91-8e46-2c9e3c456c28i︠
%md
Und dann effizient:
︡2d966fc4-f3ad-4413-afad-3927783651ef︡{"html":"<p>Und dann effizient:</p>\n"}︡
︠2ace9b3a-a6e5-48d8-b5a8-ecf1f8fa7cea︠
%timeit Mod(11,101)^m
︡21e84050-6359-4aa4-9975-00579872ca21︡{"stdout":"625 loops, best of 3: 6.12 µs per loop\n"}︡
︠1b4970ac-ef2e-4586-b1aa-924d32ec0e84i︠
%md
Die python-Funktion `pow(a,m,n)` berechnet auch $a^m \pmod{n}$ effizient.

Beispiele:
︡246d5d27-1e3f-4fcc-8a56-2f0584a4f4d1︡{"html":"<p>Die python-Funktion <code>pow(a,m,n)</code> berechnet auch $a^m \\pmod{n}$ effizient.</p>\n\n<p>Beispiele:</p>\n"}︡
︠387e36e1-a120-4587-b0d0-6e4f97e71611︠
pow(3,2,5)
︡26cdbd8e-6a3f-4135-9b00-6885d527545d︡{"stdout":"4\n"}︡
︠798c67d7-86a7-46e1-8f3a-64bf704b572a︠
(3^2) % 5
︡3e28e190-16d7-4528-a917-b3f680c85f8b︡{"stdout":"4\n"}︡
︠d73623d1-255e-40d4-ba5d-8c156efa52ce︠
type(pow(3,2,5))
︡3c87633c-054a-4d93-b2a8-3b11396c03fd︡{"stdout":"<type 'sage.rings.finite_rings.integer_mod.IntegerMod_int'>\n"}︡
︠36e982b0-2b75-4c2a-9a71-16c12f11c6e0︠
pow(3,2,5) == Mod(3,5)^2
︡46d59a4d-0131-4bc1-80f0-9852b4aead78︡{"stdout":"True\n"}︡
︠cf06d33d-719d-4283-9ed9-6a060a11df12︠
type(Mod(3,5)^2)

︡4901dcc3-0015-4d76-b88b-bc576610952e︡{"stdout":"<type 'sage.rings.finite_rings.integer_mod.IntegerMod_int'>\n"}︡
︠52de4522-f88e-4998-9598-d2d461d0268ci︠
%md
Es handelt sich hier also in beiden Fällen um einen `sage`-eigenen Datentyp fuer das Rechnen Modulo $n$.

Schauen wir mal, ob dies auch gleich schnell ist:
︡6e75450f-a0bb-4ed8-b7f8-8ddb88c4f052︡{"html":"<p>Es handelt sich hier also in beiden Fällen um einen <code>sage</code>-eigenen Datentyp fuer das Rechnen Modulo $n$.</p>\n\n<p>Schauen wir mal, ob dies auch gleich schnell ist:</p>\n"}︡
︠0467b6e9-c963-476d-b753-d3d9d27bf605︠
%timeit pow(11,m,101)
%timeit Mod(11,101)^m
︡7401bdbe-8f00-42d3-a14a-d4c5fda3be44︡{"stdout":"625 loops, best of 3: 10.2 µs per loop\n"}︡{"stdout":"625 loops, best of 3: 6.46 µs per loop\n"}︡
︠e3fefa8b-0ac0-4050-aaae-a309a803c81ci︠
%md
`pow` scheint langsamer zu sein.
Wir probieren mal noch deutlich größere Beispiele:
︡47dadfae-5377-4cc9-a2e2-2fdd5012be15︡{"html":"<p><code>pow</code> scheint langsamer zu sein.\nWir probieren mal noch deutlich größere Beispiele:</p>\n"}︡
︠3b4bc5d3-f568-461a-9452-1c620aee0457︠
%timeit pow(11,10000*m,1011111123232323)
%timeit Mod(11,1011111123232323)^(10000*m)
︡1f2eed6b-003d-4b22-a2b5-c8ee74e6fda6︡{"stdout":"625 loops, best of 3: 11.5 µs per loop\n"}︡{"stdout":"625 loops, best of 3: 8.16 µs per loop\n"}︡
︠4c6e73f2-fbe7-4158-b964-8dbe24e178b9︠
pow(11,10000*m,1011111123232323)
pow(11,10000*m,1011111123232323) == Mod(11,1011111123232323)^(10000*m)
︡26baaa55-0387-4638-abb6-a6e926b175c7︡{"stdout":"896752415098099\n"}︡{"stdout":"True\n"}︡
︠d4125a3d-fc97-42ce-8bac-f663e504d6adi︠
%md
Also irgendwie ein leichter Vorteil für das Verwenden von `Mod`.

Warum ist mir nicht ganz klar.
︡c7e90f69-46c5-4f27-92e5-eac77dcbd880︡{"html":"<p>Also irgendwie ein leichter Vorteil für das Verwenden von <code>Mod</code>.</p>\n\n<p>Warum ist mir nicht ganz klar.</p>\n"}︡
︠b01b3bf1-7f93-49f4-8e39-bf050fcfdfcai︠
%md
***Wir implementieren das Ganze jetzt mal selbst.***

Dabei achten wir mal etwas darauf, dass wir überprüfen, ob die Eingaben den korrekten Typ haben.

Ich demonstriere dabei mal gute Praktikten beim Programmieren.

Hinweis: Das schnelle `assert a in ZZ` sollte man eher nicht verwenden (ja, habe ich auch schon gemacht...)

Das `assert`-Statement ist eigentlich dazu da, um Programmfehler zu vermeiden, aber nicht, um Userinput zu überprüfen.
︡871d4f70-df58-44b5-a3b8-60929b224755︡{"html":"<p><strong><em>Wir implementieren das Ganze jetzt mal selbst.</em></strong></p>\n\n<p>Dabei achten wir mal etwas darauf, dass wir überprüfen, ob die Eingaben den korrekten Typ haben.</p>\n\n<p>Ich demonstriere dabei mal gute Praktikten beim Programmieren.</p>\n\n<p>Hinweis: Das schnelle <code>assert a in ZZ</code> sollte man eher nicht verwenden (ja, habe ich auch schon gemacht&#8230;)</p>\n\n<p>Das <code>assert</code>-Statement ist eigentlich dazu da, um Programmfehler zu vermeiden, aber nicht, um Userinput zu überprüfen.</p>\n"}︡
︠4e001aa7-b07b-4c7c-91f5-67567992fc07︠
def as_binary(a):
    r"""
    Compute a binary presentation of the positive integer $a$.
    """
    #b = a
    a = Integer(a) # das ist der effektivste Weg, sicherzustellen, dass wir nur eine ganze Zahl bekommen
    if not a > 0:
        raise ValueError("a has to be positive")
    i = 0
    eps = []
    while a != 0:
        if a % 2 == 1:
            eps.append(1)
        else:
            eps.append(0)
        i += 1
        a = floor(a/2)
    #assert sum(eps[i]*2^i for i in range(len(eps))) == b
    return eps
︡5a62ff8f-bf51-4dee-9a25-7ed68c200577︡
︠33bcd85c-ac5e-430e-a91c-39d76022c2f0i︠
%md
Ich habe absichtlich `Integer(a)` am Anfang verwendet.
Man könnte natürlich auch dies machen:
```
if not a in ZZ:
    raise TypeError("a has to be an Integer")
```

Der Nachteil ist: weniger Felixibilität, wie das folgende Beispiel zeigt.
︡1a14b070-4f63-4ae3-aaa3-93452ecc558d︡{"html":"<p>Ich habe absichtlich <code>Integer(a)</code> am Anfang verwendet.\nMan könnte natürlich auch dies machen:\n<code>\nif not a in ZZ:\n    raise TypeError(\"a has to be an Integer\")\n</code></p>\n\n<p>Der Nachteil ist: weniger Felixibilität, wie das folgende Beispiel zeigt.</p>\n"}︡
︠cc45c6c0-b3b1-4a1c-b314-7481ec293fd8︠
'5' in ZZ
︡373c8472-feb6-4c0d-8f5d-c03b0ce2a8ce︡{"stdout":"False\n"}︡
︠63a55190-866d-4fd9-9c27-30be4f944cd6︠
Integer('5')
︡47206cb7-2495-48e6-96b4-1e3e36e5160a︡{"stdout":"5\n"}︡
︠cf54c9c9-e792-4020-a6f4-cd0e6a044320i︠
%md
So sieht es aus, wenn man nun eine laut unserer Spezifikation ungültige Eingabe macht:
︡dfbe593d-38af-4824-a990-584571683101︡{"html":"<p>So sieht es aus, wenn man nun eine laut unserer Spezifikation ungültige Eingabe macht:</p>\n"}︡
︠91865e32-07c2-497b-b705-b14fcbe3ad2f︠
as_binary(-8)
︡468db643-ff00-437b-9d34-6b8f0828e395︡{"stderr":"Error in lines 1-1\nTraceback (most recent call last):\n  File \"/projects/6931ad98-daf6-4573-a651-563956d3599c/.sagemathcloud/sage_server.py\", line 733, in execute\n    exec compile(block+'\\n', '', 'single') in namespace, locals\n  File \"\", line 1, in <module>\n  File \"\", line 13, in as_binary\nValueError: a has to be positive\n"}︡
︠4478e9d0-29f4-4d1f-a677-3d0401178d21︠
a = as_binary(1001); a
︡410e16f0-7c33-425c-a058-1956c5b3ebb8︡{"stdout":"[1, 0, 0, 1, 0, 1, 1, 1, 1, 1]\n"}︡
︠ba347855-da95-476d-a60a-a95779f4c6dai︠
%md
Das geht auch:
︡eaa4c46c-815f-46bf-b7da-f9d8029c1d08︡{"html":"<p>Das geht auch:</p>\n"}︡
︠93397a28-c8de-482d-a566-a38211758f8d︠
as_binary(10.0000)
︡722ea173-bbe0-47d9-8c27-c796439e9d2b︡{"stdout":"[0, 1, 0, 1]\n"}︡
︠d4a54cf7-d155-462e-974e-608b90476377︠
def power_mod(a,m,n):
    r"""
    Compute a^m modulo n.
    """
    a = Integer(a)
    m = Integer(m)
    if n <= 1:
        raise ValueError("n should be >= 2")
    if m == 0:
        return 1
    if m < 0:
        m = -m
        a = Mod(a,n)**-1
    if a == 0:
        return 0
    n = Integer(n)
    bm = as_binary(m)
    powers = [a]
    for i in range(len(bm)-1):
        powers.append((powers[-1]^2) % n)
    #print powers
    res = 1
    for i in range(len(bm)):
        if bm[i] == 1:
            res = (res*powers[i]) % n
        #print i, res, bm[i], powers[i]
    return res
︡4b58f158-d108-48d3-9cff-8f6a6eb4eb35︡
︠1352e1b0-a988-46ba-8a93-999e98d7dfcf︠
power_mod('a',int(2),int(3))
︡738aca32-8e77-4f11-a592-7d8068f014bb︡{"stderr":"Error in lines 1-1\nTraceback (most recent call last):\n  File \"/projects/6931ad98-daf6-4573-a651-563956d3599c/.sagemathcloud/sage_server.py\", line 733, in execute\n    exec compile(block+'\\n', '', 'single') in namespace, locals\n  File \"\", line 1, in <module>\n  File \"\", line 5, in power_mod\n  File \"integer.pyx\", line 696, in sage.rings.integer.Integer.__init__ (sage/rings/integer.c:7714)\nTypeError: unable to convert x (=a) to an integer\n"}︡
︠c9a84f59-6a93-4648-8701-16bfad214ba9︠
%timeit power_mod(11,m*1000,101)
︡3da3e9ed-a8e0-46ad-a056-7f5ab3e20e9c︡{"stdout":"625 loops, best of 3: 148 µs per loop"}︡{"stdout":"\n"}︡
︠65945f58-0c00-4aab-a8fe-a9dd4ac836e6︠
%timeit Mod(11,100)^(m*1000)
︡f64367c8-23db-4df6-8fd4-84c6b393a916︡{"stdout":"625 loops, best of 3: 7.84 µs per loop\n"}︡
︠4b2c47ab-3e6d-4821-8c43-f845c0934d8ai︠
%md
Unsere Implementation ist langsamer (aber trotzdem viel schneller als Exponenzieren und dann reduzieren)

***Jetzt testen wir unsere Implementation erst einmal auf Korrektheit!***
︡68c5b376-886f-4a44-83c4-5d62d2da20df︡{"html":"<p>Unsere Implementation ist langsamer (aber trotzdem viel schneller als Exponenzieren und dann reduzieren)</p>\n\n<p><strong><em>Jetzt testen wir unsere Implementation erst einmal auf Korrektheit!</em></strong></p>\n"}︡
︠55e8d0a1-e9eb-4f14-b3e6-5f6b4e4beec9︠
def test_power_mod(N, pm = power_mod, m_range = range(200), lower = 2,  debug = False):
    if not N > 1:
        raise TypeError("N has to be greater than {0}.".format(lower))
    err = False
    for n in range(lower,lower+N):
        for aa in range(min(n,20)):
            a = ZZ.random_element(n)
            for m in m_range:
                if debug:
                    print "n = {n}, a = {a}, m = {m}".format(n=n,a=a,m=m)
                r = Mod(a,n)^m
                s = pm(a,m,n)
                if debug:
                    print r,s
                if not r == s:
                    print "Error: {a}^{m} mod {n} = {r} but power_mod({a},{m},{n}) returned {s}".format(a=a,m=m,n=n,r=r,s=s)
                    err = True
    if not err:
        print "Test passed."
︡4009a861-d693-4832-b264-bca54183f1f9︡
︠7621998d-b777-4377-ad8f-78257ad61b83︠
%time test_power_mod(100)
︡c2bf364d-60b3-4a4b-83a4-ba1018106edb︡{"stdout":"Test passed."}︡{"stdout":"\n"}︡{"stdout":"CPU time: 15.48 s, Wall time: 15.45 s\n"}︡
︠fa745a3c-e892-4910-8118-c90cd784b927i︠
%md
Noch eine Variation:
︡e6c7b014-1446-4fa6-97c1-71ff2127d746︡{"html":"<p>Noch eine Variation:</p>\n"}︡
︠89530f07-e158-455e-9074-740cf296d189︠
def power_mod_new(a,m,n):
    r"""
    Compute a^m modulo n.
    """
    a = Integer(a)
    m = Integer(m)
    n = Integer(n)
    bm = as_binary(m)
    power = a
    res = 1 if bm[0] == 0 else a
    for i in range(len(bm)-1):
        power = (power^2) % n
        if bm[i+1] == 1:
            res = (res*power) % n
        #print i, res, bm[i+1], power
    return res
︡aefd8e41-36ca-4003-92e1-a392b6580118︡
︠e0fd358f-c36e-49c9-84ad-edd9d314cc79︠
%timeit power_mod_new(11,m*1000,101)
︡80fbbc29-302d-47e5-b4d9-6ebb5a9067f9︡{"stdout":"625 loops, best of 3: 144 µs per loop"}︡{"stdout":"\n"}︡
︠9a2382cc-1d5f-4cf7-a206-d32c8ec52a07i︠
%md
Kein Geschwindigkeitsvorteil
︡635e5090-0f8e-4b5f-9343-528c7158bec5︡{"html":"<p>Kein Geschwindigkeitsvorteil</p>\n"}︡
︠0d4f86a9-0c43-49f1-8c51-7ab2e67010eei︠
%md
## Etwas für Fortgeschrittene als kleines Extra:
### Jetzt machen wir unsere eigene ganz schnelle Implementation mit `cython`
```Cython gives you the combined power of Python and C```

Siehe [http://cython.org/](http://cython.org/)

Dabei können wir eine Mischung aus C-Code und python schreiben und es wird reiner C-Code erzeugt und im Hintergrund kompiliert.

Im Ergebnis können wir den C-Code aber direkt aus Sage / python aufrufen!

Ich benutze im Folgenden eine Bibliothek für ganzzahlige Arithmetik, die für beliebig große ganze Zahlen funktioniert
und nicht an die maximal 64 bit des Typs `long long` von C gebunden ist.
Für den Input nehme ich trotzdem mal der Einfachheit halber `long long`,
das sollte uns zur Demonstration reichen. Aber beim Quadrieren auf dem Wege entstehen dann schnell Overflows,
die wir so vermeiden.
︡8bfa87fa-0348-4a55-ae52-251cc33da9ec︡{"html":"<h2>Etwas für Fortgeschrittene als kleines Extra:</h2>\n\n<h3>Jetzt machen wir unsere eigene ganz schnelle Implementation mit <code>cython</code></h3>\n\n<p><code>Cython gives you the combined power of Python and C</code></p>\n\n<p>Siehe <a href=\"http://cython.org/\">http://cython.org/</a></p>\n\n<p>Dabei können wir eine Mischung aus C-Code und python schreiben und es wird reiner C-Code erzeugt und im Hintergrund kompiliert.</p>\n\n<p>Im Ergebnis können wir den C-Code aber direkt aus Sage / python aufrufen!</p>\n\n<p>Ich benutze im Folgenden eine Bibliothek für ganzzahlige Arithmetik, die für beliebig große ganze Zahlen funktioniert\nund nicht an die maximal 64 bit des Typs <code>long long</code> von C gebunden ist.\nFür den Input nehme ich trotzdem mal der Einfachheit halber <code>long long</code>, \ndas sollte uns zur Demonstration reichen. Aber beim Quadrieren auf dem Wege entstehen dann schnell Overflows,\ndie wir so vermeiden.</p>\n"}︡
︠23aaa46c-299a-407d-99c4-52d443fed4b8︠
%cython
from sage.all import Integer, Mod
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
from libc.math cimport log, floor, ceil
from sage.libs.gmp.all cimport *

cdef unsigned short* as_binary_cython(long a, long l):
    cdef int i = 0
    cdef double b = <double>a
    if l == -1:
        l = <long>floor(ceil(log(a)/log(2))+1)
    cdef unsigned short *eps = <unsigned short *>PyMem_Malloc(l * sizeof(unsigned short))
    if eps is NULL:
        raise MemoryError("Could not allocate memory")
    for i in range(l):
        eps[i] = 0
    i = 0
    if not eps:
        raise MemoryError()
    while b != 0:
        if b % 2 == 1:
            eps[i] = 1
        else:
            eps[i] = 0
        #print i, eps[i]
        i += 1
        b = <long>floor(b/2)
    return eps

cpdef power_mod_cython(long a, long m, long long n):
    r"""
    Compute a^m modulo n.
    Here, a,m and n are non-negative integers.
    """
    if m == 0:
        return 1
    if m < 0:
        m = -m
        a = Mod(a,n)**-1
    if n == 0:
        raise ValueError("n has to be positive.")
    if a == 0:
        return 0
    cdef long l = <long>floor(ceil(log(m)/log(2))+1)
    cdef unsigned short *bm = as_binary_cython(m,l)

    cdef mpz_t mod,two,res,power
    mpz_init(mod)
    mpz_init(two)
    mpz_init(res)
    mpz_init(power)
    mpz_set_si(mod,n)
    mpz_set_si(power,a)
    mpz_set_ui(two,2)
    cdef int i = 0
    mpz_set_si(res, a % n if bm[0] == 1 else 1)

    for i in range(l-1):
        mpz_powm(power,power,two,mod)
        if bm[i+1] == 1:
            mpz_mul(res,res,power)
            mpz_mod(res,res,mod)

    return mpz_get_si(res)
︡6b15f9ce-fadc-4ce6-ae2c-2a13657973d9︡{"html":"<a href='/6931ad98-daf6-4573-a651-563956d3599c/raw/.sage/temp/compute17dc0/13685/spyx/_projects_6931ad98_daf6_4573_a651_563956d3599c__sage_temp_compute17dc0_13685_dir_bX5X6X_a_pyx/_projects_6931ad98_daf6_4573_a651_563956d3599c__sage_temp_compute17dc0_13685_dir_bX5X6X_a_pyx_0.html' target='_new' class='btn btn-small' style='margin-top: 1ex'>Auto-generated code... &nbsp;<i class='fa fa-external-link'></i></a>"}︡
︠1e0f86ad-89a5-4d08-9d20-f129fbfee38a︠
%timeit power_mod_cython(11,m*10000,101)
︡8df2f548-33b0-4551-aee7-dd43d228a707︡{"stdout":"625 loops, best of 3: 11.8 µs per loop\n"}︡
︠aea0abda-fca6-488d-8c7e-708298982a37︠
%timeit Mod(11,101)^(10000*m)
︡4b671f45-827b-41a6-9f0f-c249919db497︡{"stdout":"625 loops, best of 3: 6.97 µs per loop\n"}︡
︠a92e6f4a-a1fa-4dee-8e08-d335c3412130i︠
%md
Wow, wir sind ja fast so gut, wie `Mod`!

Ein paar Tests.
︡de409e11-8d26-407d-8f81-33ab8531e5a8︡{"html":"<p>Wow, wir sind ja fast so gut, wie <code>Mod</code>!</p>\n\n<p>Ein paar Tests.</p>\n"}︡
︠b1c43e32-484d-469c-805d-6e6fc00e91cd︠
power_mod_cython(11,-10,101)
︡5a39574c-32a7-4f5f-80fc-221dfd08e54d︡{"stdout":"6\n"}︡
︠8b022f87-2b1a-4b30-a69b-344a5999c82b︠
%timeit power_mod_cython(1211,m*10000,1012221212151121217)
︡0f30e49b-0144-4876-ad2c-124dc639495a︡{"stdout":"625 loops, best of 3: 11.5 µs per loop\n"}︡
︠7eecaef6-34df-4caa-bfdc-f654eaa228e2︠
power_mod_cython(1211454,m*10000,1012221212151121217) == Mod(1211454,1012221212151121217)^(m*10000)
︡5aa9dc8b-bd45-4b5b-9685-0f0354ec51e7︡{"stdout":"True\n"}︡
︠8b2575d3-57e0-4c39-97a6-c748bfbfed7c︠
%timeit Mod(1211454,1012221212151121217)^(m*10000)
︡59bad90f-f036-4125-9c8b-1c60e267b44f︡{"stdout":"625 loops, best of 3: 7.52 µs per loop\n"}︡
︠a1bc0b6f-f17c-4922-bd6f-9d3b37cf062c︠
n=10122212112
︡3fb941ed-80b8-4ae0-8030-959f41a10d7d︡
︠50ee8588-169f-497c-afbb-40b028399cab︠
power_mod(1211,m,n)
︡e00cf899-edc9-4673-8102-8f66a9e223ce︡{"stdout":"6861095315\n"}︡
︠60da9a1c-37d4-4668-9dd9-ac3c3798183e︠
power_mod_new(1211,m,n)
︡080dc19a-2e38-4710-9845-a0e0b0acd552︡{"stdout":"6861095315\n"}︡
︠aa5e10c3-114e-49e5-bd3b-a8b172b057dc︠
power_mod_cython(1211,m,n)
︡397db494-402a-42ff-a487-e5ef5c7a6e0b︡{"stdout":"6861095315\n"}︡
︠717da718-da74-41b9-a855-8a78db888b8b︠
power_mod(1211,m,10122212112)
︡07603811-d9f1-405d-a009-271d62d6966e︡{"stdout":"6861095315"}︡{"stdout":"\n"}︡
︠4a0a8b6d-d35b-417c-8b82-9e8278885935︠
Mod(1211,10122212112)^(m)
︡ebedf6ce-5434-43a0-888b-20e206b1adb1︡{"stdout":"6861095315\n"}︡
︠4ed3f4c8-16e7-4f20-835d-fd8281d04a2f︠
(1211^m) % 10122212112
︡2855e62b-3a11-4e4f-a36a-837e79ce108c︡{"stdout":"6861095315"}︡{"stdout":"\n"}︡
︠a40d7a7e-69db-4679-9ae4-2eff5a2feb1a︠
power_mod_cython(1211,-3,1031)
︡d84aa255-a7b2-4c2c-8d71-2753917c94fb︡{"stdout":"486\n"}︡
︠30a35e17-d105-4921-999d-0a34e90227da︠
%time test_power_mod(100,power_mod_cython, m_range = range(m,m+100), lower=25870251711100011)
︡e4028374-529e-4568-a8e6-3c13a2826762︡{"stdout":"Test passed."}︡{"stdout":"\n"}︡{"stdout":"CPU time: 3.83 s, Wall time: 3.82 s\n"}︡
︠5cf79980-9b54-4f0b-a44f-45a505ab8aa9︠









