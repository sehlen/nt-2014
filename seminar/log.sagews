︠67c2878a-f768-4de3-be22-ba4381938748i︠
%md
## 14.04: Protokoll
  - Es gibt eine Google Group [tud-nt-ss14](https://groups.google.com/forum/#!forum/tud-nt-ss14/),
    zu der ich Alle Teilnehmer hinzugefügt habe.
  - Einführung in [Sage](http://www.sagemath.org), [SageMathCloud](http://www.sagemath.com)
  - Überblick über die (möglichen) Themen
  - Anleitung: Jeder Teilnehmer erstellt einen Account und ein Projekt zu dem Seminar
    --> mich als "Collaborator" hinzufügen: Klick auf die Zange...
  - Terminplanung:
    - Raum: 401, ggf auch PC Pool im Mathebau
    - Angenommener Vorschlag: Montag, der 5.5., 26.5., 16.6., 14.7.
    - Verteilung der Vorträge:
      1. Pia, Susanne
      2. Albert, Fei
      3. Johannes, Ellen, Sabrina
      4. Vanessa
    - Das Seminar beginnt um 11:40 Uhr in Raum 401.
    - Das Seminar endet voraussichtlich um 16:05 Uhr.

︡4fa70487-7f5e-41b9-82cb-4d6a16595748︡{"html":"<h2>14.04: Protokoll</h2>\n\n<ul>\n<li>Es gibt eine Google Group <a href=\"https://groups.google.com/forum/#!forum/tud-nt-ss14/\">tud-nt-ss14</a>,\nzu der ich Alle Teilnehmer hinzugefügt habe.</li>\n<li>Einführung in <a href=\"http://www.sagemath.org\">Sage</a>, <a href=\"http://www.sagemath.com\">SageMathCloud</a></li>\n<li>Überblick über die (möglichen) Themen</li>\n<li>Anleitung: Jeder Teilnehmer erstellt einen Account und ein Projekt zu dem Seminar\n&#8211;> mich als &#8220;Collaborator&#8221; hinzufügen: Klick auf die Zange&#8230;</li>\n<li>Terminplanung:\n<ul>\n<li>Raum: 401, ggf auch PC Pool im Mathebau</li>\n<li>Angenommener Vorschlag: Montag, der 5.5., 26.5., 16.6., 14.7.</li>\n<li>Verteilung der Vorträge:\n<ol>\n<li>Pia, Susanne</li>\n<li>Albert, Fei</li>\n<li>Johannes, Ellen, Sabrina</li>\n<li>Vanessa</li>\n</ol></li>\n<li>Das Seminar beginnt um 11:40 Uhr in Raum 401.</li>\n<li>Das Seminar endet voraussichtlich um 16:05 Uhr.</li>\n</ul></li>\n</ul>\n"}︡
︠96360689-fba5-4e83-8a9e-07e4d16266e6i︠
%md
### Zu den Blöcken:
- Jeweils 2 Vorträge und ein Praxis-Teil

1. Primzahlen, Riemannsche Vermutung
2. $\mathbb{Z}/n\mathbb{Z}$, quadratische Reste, Primzahltests, Public-Key-Verschlüsselung: Diffie-Hellman, RSA
3. Elliptische Kurven und Kryptographie
4. Mehrere Möglichkeiten, zum Beispiel der Zusammenhang von: Elliptische Kurven und Fermats letzter Satz, Modulformen und die BSD-Vermutung
︡76055fc1-52b1-470b-8b25-8305132c474d︡{"html":"<h3>Zu den Blöcken:</h3>\n\n<ul>\n<li>Jeweils 2 Vorträge und ein Praxis-Teil</li>\n</ul>\n\n<ol>\n<li>Primzahlen, Riemannsche Vermutung</li>\n<li>$\\mathbb{Z}/n\\mathbb{Z}$, quadratische Reste, Primzahltests, Public-Key-Verschlüsselung: Diffie-Hellman, RSA</li>\n<li>Elliptische Kurven und Kryptographie</li>\n<li>Mehrere Möglichkeiten, zum Beispiel der Zusammenhang von: Elliptische Kurven und Fermats letzter Satz, Modulformen und die BSD-Vermutung</li>\n</ol>\n"}︡
︠1eab778a-c1e0-4aa6-8c8f-cd2cf1d0c531︠
︡c4090bae-1c75-4e8b-aaf2-2149dd38961b︡
︠02a95e90-8dd9-4efc-96d6-ce784911fc81︠









