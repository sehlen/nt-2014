︠741592e4-4abd-4ea9-8c76-070b4d29a2c2i︠
%md
# Einführung
## Ziele des Seminars
- Einführung in die Zahlentheorie, "elementare" und weiterführende Themen
- Fokus: explizite Methoden, ganz konkretes Arbeiten mit den Objekten (am Computer)
- Lernen der Benutzung von Sage, der SageMathCloud, $\LaTeX$ und weiterer technischer Hilfsmittel

︡4a11b310-4a0f-4eac-a864-83ac9d7ebb06︡{"html":"<h1>Einführung</h1>\n\n<h2>Ziele des Seminars</h2>\n\n<ul>\n<li>Einführung in die Zahlentheorie, &#8220;elementare&#8221; und weiterführende Themen</li>\n<li>Fokus: explizite Methoden, ganz konkretes Arbeiten mit den Objekten (am Computer)</li>\n<li>Lernen der Benutzung von Sage, der SageMathCloud, $\\LaTeX$ und weiterer technischer Hilfsmittel</li>\n</ul>\n"}︡
︠89d572ce-fdf2-449a-aafe-89904c20b242i︠
%md
## Sage und die SageMathCloud(SMC)
![Sage logo](http://www.sagemath.org/pix/sage_logo_new.png "Sage")

[Sage](http://www.sagemath.org/) wurde von [William Stein](http://www.wstein.org)
als Projekt mit dem Ziel begonnen:

> Creating a viable free open source alternative to Magma, Maple, Mathematica and Matlab.

![William Stein](http://wstein.org/wstein64.png "William Stein")
William Stein, Professor an der University of Washington.

- Sage ist im Grunde eine Distribution:
  - Es sind viele andere Open-Source Software-Pakete eingebunden und werden mitgeliefert:
    z.B. Pari/Gp, Maxima, GAP...
- Benutzt wird haupsächlich Python als Programmiersprache
- Dies ist eine Interpreter-Programmiersprache
- sehr sehr flexibel
- leider nicht so schnell
- Kann aber Bibliotheken einbinden
- Und/oder cython benutzen
- Die SageMathCloud ist ziemlich neu und wird sehr rapide entwickelt
- Unglaublich viele Möglichkeiten:
  - Sage vorinstalliert und immer aktuell
  - ... so kann man sofort loslegen und muss nichts installieren
  - voller Zugriff auf ein Terminal (eine Shell)
  - $\LaTeX$-Editor mit PDF-Vorschau und Vorwärts-/und Rückwärtssuche
  - Worksheets unterstützen HTML, Markdown und $\LaTeX$
  - Gemeinsames Arbeiten an einem Dokument
  - ...
︡61d0bbf8-9e8e-4e1f-843d-a4935e0ff388︡{"html":"<h2>Sage und die SageMathCloud(SMC)</h2>\n\n<p><img src=\"http://www.sagemath.org/pix/sage_logo_new.png\" alt=\"Sage logo\" title=\"Sage\" /></p>\n\n<p><a href=\"http://www.sagemath.org/\">Sage</a> wurde von <a href=\"http://www.wstein.org\">William Stein</a>\nals Projekt mit dem Ziel begonnen:</p>\n\n<blockquote>\n  <p>Creating a viable free open source alternative to Magma, Maple, Mathematica and Matlab.</p>\n</blockquote>\n\n<p><img src=\"http://wstein.org/wstein64.png\" alt=\"William Stein\" title=\"William Stein\" />\nWilliam Stein, Professor an der University of Washington.</p>\n\n<ul>\n<li>Sage ist im Grunde eine Distribution:\n<ul>\n<li>Es sind viele andere Open-Source Software-Pakete eingebunden und werden mitgeliefert:\nz.B. Pari/Gp, Maxima, GAP&#8230;</li>\n</ul></li>\n<li>Benutzt wird haupsächlich Python als Programmiersprache</li>\n<li>Dies ist eine Interpreter-Programmiersprache</li>\n<li>sehr sehr flexibel</li>\n<li>leider nicht so schnell</li>\n<li>Kann aber Bibliotheken einbinden</li>\n<li>Und/oder cython benutzen</li>\n<li>Die SageMathCloud ist ziemlich neu und wird sehr rapide entwickelt</li>\n<li>Unglaublich viele Möglichkeiten:\n<ul>\n<li>Sage vorinstalliert und immer aktuell</li>\n<li>&#8230; so kann man sofort loslegen und muss nichts installieren</li>\n<li>voller Zugriff auf ein Terminal (eine Shell)</li>\n<li>$\\LaTeX$-Editor mit PDF-Vorschau und Vorwärts-/und Rückwärtssuche</li>\n<li>Worksheets unterstützen HTML, Markdown und $\\LaTeX$</li>\n<li>Gemeinsames Arbeiten an einem Dokument</li>\n<li>&#8230;</li>\n</ul></li>\n</ul>\n"}︡
︠a9d0b4a4-a247-4a6f-9c29-7c573cec0b2ei︠
︡353720e5-fa08-4c25-a7be-cef04138d2ba︡{"html":"<h2>Primzahlen</h2>\n\n<p><strong>Theorem 1:</strong> (Fundamentalsatz der Arithmetik)\nJede natürliche Zahl kann als Produkt von Primzahlen geschrieben werden.\nDie Darstellung ist bis auf die Reihenfolge eindeutig.</p>\n\n<p><strong>Theorem 2:</strong> (Euklid)\nEs gibt unendlich viele Primzahlen.</p>\n\n<h3>Die derzeit <em>größte bekannte</em> Primzahl:</h3>\n\n<p>Stand im Januar 2014: $2^{57 885 161} − 1$, dies ist eine Zahl mit $17.425.170$ Dezimalstellen.\nSiehe <a href=\"http://en.wikipedia.org/wiki/Largest_known_prime_number\">Wikipedia</a>.</p>\n\n<p>Dies ist eine sogenannte <strong>Mersenne-Primzahl</strong> (d.h. von der Form $2^n-1$).</p>\n\n<p><em>Geldquelle</em>: Finde eine Primzahl mit 100.000.000 Dezimalstellen und erhalte <a href=\"https://www.eff.org/awards/coop\">150.000$</a>.</p>\n"}︡
︠a43c565a-8ca9-42c2-b924-943fc760b8b0︠
p = 2^57885161 - 1
︡9321db65-abee-4bda-88a2-62da56ec2eaf︡
︠ad84a3be-27c1-4cf6-a16e-73b236d3ee76︠
s = str(p)
len(s)
︡7ec95179-c68b-457b-80f3-09f22a3d274f︡{"stdout":"17425170\n"}︡
︠6a79ea80-d425-4922-a1a0-494bc370bfb3︠
s[-15:]
︡3bc5864a-cb4a-48ae-b85f-822c45a7971e︡{"stdout":"'988071724285951'\n"}︡
︠0d87c20f-60f4-4b98-a5da-c5a3e99bd84d︠
s[:10] + '...' + s[-10:]
︡b7668804-d918-4e1f-b1e1-2d2a168163c1︡{"stdout":"'5818872662...1724285951'\n"}︡
︠2b996d7f-9d55-4c3b-b9dd-97b6b3fd9061i︠
%md
## Ein Sage *interact* zur Primanzahlfunktion $\pi(x)$
Definiere
\[
  \pi(x) = |\{n \leq x\ \mid\ n \text{ ist Primzahl}\}|.
\]
︡b80f6d1a-80ca-49b8-9072-8ff098f28890︡{"html":"<h2>Ein Sage <em>interact</em> zur Primanzahlfunktion $\\pi(x)$</h2>\n\n<p>Definiere\n\\[\n  \\pi(x) = |\\{n \\leq x\\ \\mid\\ n \\text{ ist Primzahl}\\}|.\n\\]</p>\n"}︡
︠9c077629-e04d-48b2-ae0e-bcf9952acf09︠
@interact
def f(a=0, b=50, PNT=False, Gauss=False):
    x = var('x')
    g = prime_pi.plot(a,b)
    if PNT:
        g += plot(x/(log(x)-1), (max(a,5),b), color='red')
    if Gauss:
        g += plot(Li, (max(a,2),b), color='darkgreen')
    show(g, gridlines=True, svg=True, frame=True)
︡1322a154-d065-4f48-a584-94f384d95289︡{"interact":{"style":"None","flicker":false,"layout":[[["a",12,null]],[["b",12,null]],[["PNT",12,null]],[["Gauss",12,null]],[["",12,null]]],"id":"1d80e95d-2bf4-4b3f-8dc5-b712b3ffb863","controls":[{"control_type":"input-box","default":0,"label":"a","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"a","type":null},{"control_type":"input-box","default":50,"label":"b","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"b","type":null},{"default":false,"var":"PNT","readonly":false,"control_type":"checkbox","label":"PNT"},{"default":false,"var":"Gauss","readonly":false,"control_type":"checkbox","label":"Gauss"}]}}︡
︠4ad25ef6-39cf-455e-8818-286b1327c9e0i︠
%md
**Der Primzahlsatz (Prime Number Theorem):** Es gilt
\[
  \pi(x) \sim \frac{x}{\ln(x)}
\]
︡0ca0a863-43d1-44f4-80c8-65bf9f023d97︡{"html":"<p><strong>Der Primzahlsatz (Prime Number Theorem):</strong> Es gilt\n\\[\n  \\pi(x) \\sim \\frac{x}{\\ln(x)}\n\\]</p>\n"}︡
︠3ca4e8f3-431b-49f9-b9a4-79fb48b07fa3︠
︠5075e9a5-2d6c-453c-a3fc-2a973e76e251︠
︡4b98f5b3-cab1-43ba-9704-ae212119ca07︡{"html":"<h2>Kurze Einführung in Python und Sage</h2>\n\n<h3>Zunächst zu python:</h3>\n\n<ul>\n<li><strong>Wichtig</strong>: wir benutzen Python 2.x und nicht die aktuellste Version 3.x</li>\n<li>Python 3.x ist nicht in Allem abwärtskompatibel, weshalb es (noch?) fast nicht eingesetzt wird</li>\n<li>Ein <a href=\"https://docs.python.org/2/tutorial/\">Tutorial</a></li>\n<li>Syntax: Einrücken bei Blöcken notwendig für richtiges Ausführen</li>\n<li>Standard: 4 Leerzeichen</li>\n<li>In der Cloud Einrückhilfe</li>\n<li>Keine/dynamische Typisierung (flexibel aber langsam und manchmal gefährlich)</li>\n<li>Beispiel: <code>if</code>, <code>elif</code>, <code>else</code>, <code>for</code>, <code>while</code></li>\n<li>Listen: eckige Klammern <code>[0,1,2,3]</code></li>\n<li>Dictionaries: geschweifte Klammern <code>{0: 1, 'alpha': 'beta'}</code></li>\n</ul>\n"}︡
︠f5786852-d6b6-4ab7-b89e-0b01d1ca3373︠









︠ae8b2c38-126d-4e94-a8da-48f28de6bca9︠
p = next_prime(30)
if p % 4 == 3:
    print str(p) + " ist kongruent zu 3 modulo 4"
︡4c9deb67-8966-4ea4-8c20-6e8d8c758eaa︡{"stdout":"31 ist kongruent zu 3 modulo 4\n"}︡
︠bd50df1b-3b05-4442-9c0c-ade9b35289cb︠
l1 = []
l3 = []
for p in prime_range(3,100):
    if p % 4 == 1:
        l1.append(p)
    else:
        l3.append(p)

︡260da0ec-40d4-40d2-a4e7-5ad8d60f4bf6︡
︠ac3dce9b-6567-4f09-8fe9-5331805dcc3e︠
l1
l3
︡2b5f8a16-670b-42c5-ab24-55ba898231d9︡{"stdout":"[5, 13, 17, 29, 37, 41, 53, 61, 73, 89, 97]\n"}︡{"stdout":"[3, 7, 11, 19, 23, 31, 43, 47, 59, 67, 71, 79, 83]\n"}︡
︠9576260b-9db9-46f5-9c3c-8321ceb1f7ca︠
len(l1)
len(l3)
︡f12764ef-1551-4ee8-a2be-2790a95e61a2︡{"stdout":"11\n"}︡{"stdout":"13\n"}︡
︠780669aa-9573-4389-837a-c097f2e9f74di︠
%md
- Erstellen von Listen und Dictionaries in einer Zeile
︡49589c51-8ff9-4f05-a5bc-caf47b7a9ae7︡{"html":"<ul>\n<li>Erstellen von Listen und Dictionaries in einer Zeile</li>\n</ul>\n"}︡
︠c87f896e-b5da-470d-9dac-129c96757961︠
l1 = [p for p in prime_range(3,100) if p % 4 == 1]
︡b1835b29-192d-4885-9df3-260987dd0ecd︡
︠5d9ec2d9-3187-4c0c-b201-d611f960122f︠
l1
len(l1)
︡b0061df8-3de4-41da-bdf6-75ebfbc32a81︡{"stdout":"[5, 13, 17, 29, 37, 41, 53, 61, 73, 89, 97]\n"}︡{"stdout":"11\n"}︡
︠869e7430-ba2d-4409-aeba-232a46f5ee9c︠
prime_range(10.2)
︡411c3a90-3801-4e89-9a99-64c7c3778fc3︡{"stdout":"[2, 3, 5, 7]\n"}︡
︠0a9a4def-6b7c-46e4-ad17-305d211f5300i︠
%md
# Definition von Funktionen
︡445f99d6-cf51-44a0-933e-55aa460d3baa︡{"html":"<h1>Definition von Funktionen</h1>\n"}︡
︠72d2bc49-ce1a-4c79-a3a2-ef5cb39697ff︠
def anz(t):
    ps = prime_range(3,t)
    anz1 = len([p for p in ps if p % 4 == 1])
    anz2 = len(ps)-anz1
    return anz1 - anz2
︡80369278-20fe-495f-83e8-a8ca1b942999︡
︠499bf114-553b-4d6d-a55b-ee7ee3a47c13︠
anz(10000)

︠516b7ba7-5409-4671-aa0d-d76649056b67︠
len(prime_range(10000000))
︡90a6b8ec-8d2b-4ac1-9707-cb426f0749e2︡{"stdout":"664579\n"}︡
︠9c496e04-466b-4e25-9ff7-cb86d8878be7︠
@interact
def f(t=100):
    p=plot(anz, [3,t])
    show(p, svg=True)
︡f7003403-3eec-4a05-8239-c6f2752ec9aa︡{"interact":{"style":"None","flicker":false,"layout":[[["t",12,null]],[["",12,null]]],"id":"1095f675-9d42-41d4-b069-aae8d89ae03d","controls":[{"control_type":"input-box","default":100,"label":"t","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"t","type":null}]}}︡
︠d7e978cd-ba3e-4565-bac3-fadd68887448︠
l = [0,1,2,3]
print l
︡cdddf1a4-d821-4e8b-89db-b93828a202b2︡{"stdout":"[0, 1, 2, 3]\n"}︡
︠edec2166-eb7a-4be3-bc46-8490f9c43252︠
prime_range(20)
︡bd8b2c53-ecbf-4b81-a0ae-276caa30adfb︡{"stdout":"[2, 3, 5, 7, 11, 13, 17, 19]\n"}︡
︠475228b3-a84b-48d3-93a3-c40460c6ee83︠
for a in l:
    print a
︡c17e249d-b207-4013-a2a6-f8272544fcc4︡{"stdout":"0\n1\n2\n3\n"}︡
︠48cbe502-bcb8-4be1-8eb4-0e37163505b8i︠
%md
## Sage
- Was wir hier gerade benutzen ist ein `sage-worksheet`, Dateiendung `.sagews`
- Dies ist cloud-spezifisch
- es gibt auch das Sage-Notebook, das mit der Distribution ausgeliefert wird (`.sagenb`)
- Wenn wir Code schrieben, der von anderen benutzt werden soll, schreiben wir python Dateien (`.py`) oder sage dateien (`.sage`)
- Diese können dann in einer Sage-session geladen werden
- Unterschied: `.sage` kann nur direkt in sage verwendet werden
- `.py` ist im Prinzip auch außerhalb von sage nutzbar
- `.py`: Standard, wenn wir eine Bibliothek/extra-Paket schreiben
- `.sage`: wird vom Sage-Präprozessor behandelt und zwar genau so, wie auf der Sage-Kommandozeile
- Achtung: `.py` files werden nicht vom sage Präprozessor verarbeitet
︡f21539ab-b2dd-4e68-bf8b-fa2f0f2e37c3︡{"html":"<h2>Sage</h2>\n\n<ul>\n<li>Was wir hier gerade benutzen ist ein <code>sage-worksheet</code>, Dateiendung <code>.sagews</code></li>\n<li>Dies ist cloud-spezifisch</li>\n<li>es gibt auch das Sage-Notebook, das mit der Distribution ausgeliefert wird (<code>.sagenb</code>)</li>\n<li>Wenn wir Code schrieben, der von anderen benutzt werden soll, schreiben wir python Dateien (<code>.py</code>) oder sage dateien (<code>.sage</code>)</li>\n<li>Diese können dann in einer Sage-session geladen werden</li>\n<li>Unterschied: <code>.sage</code> kann nur direkt in sage verwendet werden</li>\n<li><code>.py</code> ist im Prinzip auch außerhalb von sage nutzbar</li>\n<li><code>.py</code>: Standard, wenn wir eine Bibliothek/extra-Paket schreiben</li>\n<li><code>.sage</code>: wird vom Sage-Präprozessor behandelt und zwar genau so, wie auf der Sage-Kommandozeile</li>\n<li>Achtung: <code>.py</code> files werden nicht vom sage Präprozessor verarbeitet</li>\n</ul>\n"}︡
︠333fea70-0f71-4b71-b505-00edc2529d61i︠
%md
## Ein paar Sage-Beispiele
- Ganze Zahlen, rationale Zahlen
- Primzahlen
- Algebraische Zahlen (Intervallarithmetik)
- floating point Arithmetik
︡8a01dec8-2fea-4212-96b9-c5bfc5cb4d7a︡{"html":"<h2>Ein paar Sage-Beispiele</h2>\n\n<ul>\n<li>Ganze Zahlen, rationale Zahlen</li>\n<li>Primzahlen</li>\n<li>Algebraische Zahlen (Intervallarithmetik)</li>\n<li>floating point Arithmetik</li>\n</ul>\n"}︡
︠b8c5564d-8f31-473e-89c0-17badc3bfa3b︠
ZZ

︡706f7546-b8ff-40c3-87e0-2e3d2694b7b3︡{"stdout":"Integer Ring\n"}︡
︠45c95985-358d-43db-8284-359920532750︠
QQ
︡32f91efc-26f1-45fd-9e0d-68cebfd7abb5︡{"stdout":"Rational Field\n"}︡
︠280ca847-ab0f-4a87-8a4a-6dbcb30319cb︠
1 in ZZ
︡55804e82-7fd0-4832-ab9e-83cb8753e3f5︡{"stdout":"True\n"}︡
︠b1a8acaf-93f3-4e58-bcbc-70f57e7d55c7︠
(1/2) in QQ
︡6b495db2-03d4-4c95-86a7-70ba61b51ac1︡{"stdout":"True\n"}︡
︠acd4c290-1a32-44ec-b8b5-9b3751e9e6cb︠
4/2 in ZZ
︡afabda8f-d21f-4030-9934-0d9fab63f6a9︡{"stdout":"True\n"}︡
︠38e081ae-6ef6-467a-8041-462fbdb8c47d︠
type(4/2)
︡54201b74-f7e4-4bd7-aff0-b1461991ad71︡{"stdout":"<type 'sage.rings.rational.Rational'>\n"}︡
︠abf9678e-d6b1-4d3e-a2cf-d4c23d651ced︠
type(Integer(4/2))
︡e006ed0b-25f1-4b88-8a33-3354c5fbd3bc︡{"stdout":"<type 'sage.rings.integer.Integer'>\n"}︡
︠6a5d2a8f-3f72-4c30-a385-af4d32b4f468︠
a=QQbar(5).sqrt()
︡49a6aa41-eebc-4c4a-89a2-f0b67cb70691︡
︠0ae2fad0-e7f4-4815-b1cd-8cbe1a386349︠
a
︡22e138f0-bd1e-486a-9d3a-93a0a020ffd0︡{"stdout":"2.236067977499790?\n"}︡
︠2d5b0186-af24-4b37-b88a-676abfa478b0︠
a.minpoly()
︡778d42d6-479c-4a6c-aa6d-80f6e33abf63︡{"stdout":"x^2 - 5"}︡{"stdout":"\n"}︡
︠c87078d9-51f3-403e-b7e9-40973dde2497︠
a.n(10000)
︡38c59f8a-a221-4c31-8595-ac6767c41196︡{"stdout":"2.23606797749978969640917366873127623544061835961152572427089724541052092563780489941441440837878227496950817615077378350425326772444707386358636012153345270886677817319187916581127664532263985658053576135041753378500342339241406444208643253909725259262722887629951740244068161177590890949849237139072972889848208864154268989409913169357701974867888442508975413295618317692149997742480153043411503595766833251249881517813940800056242085524354223555610630634282023409333198293395974635227120134174961420263590473788550438968706113566004575713995659556695691756457822195250006053923123400500928676487552972205676625366607448585350526233067849463342224231763727702663240768010444331582573350589309813622634319868647194698997018081895242644596203452214119223291259819632581110417049580704812040345599494350685555185557251238864165501026243631257102444961878942468290340447471611545572320173767659046091852957560357798439805415538077906439363972302875606299948221385217734859245351512104634555504070722787242153477875291121212118433178933519103800801111817900459061884624964710424424830888012940681131469595327944789899893169157746079246180750067987712420484738050277360829155991396244891494356068346252906440832794464268088898974604630835353787504206137475760688340187908819255911797357446419024853787114619409019191368803511039763843604128105811037869895185201469704564202176389289088444637782638589379244004602887540539846015606170522361509038577541004219368498725427185037521555769331672300477826986666244621067846427248638527457821341006798564530527112418059597284945519545131017230975087149652943628290254001204778032415546448998870617799819003360656224388640963928775351726629597143822795630795614952301544423501653891727864091304197939711135628213936745768117492206756210888781887367167162762262337987711153950968298289068301825908140100389550972326150845283458789360734639611723667836657198260792144028911900899558424152249571291832321674118997572013940378819772801528872341866834541838286730027431532022960762861252476102864234696302011180269122023601581012762843054186171761857514069010156162909176398126722596559628234906785462416185794558444265961285893756485497480349011081355751416647462195183023552595688656949581635303619557453683223526500772242258287366875340470074223266145173976651742067264447621961802422039798353682983502466268030546768767446900186957209958589198316440251620919646185105744248274087229820410943710992236175285315302212109176295120886356959716907946257260325089752229704043412880822332153390119551566514079022175646165421295787804223138207855367690772666643131659319546206872064645091487274408248812817765347516867907359186246442687464199149977893991312947201459199967825762063948526250359428286402462255910378955634538283178235598391296251160036910131265905719718200181724360595512757851998329989285638604458710469334951865390330842804218272603638944541578024417457472341469729999631251094562274695974331390549780162887681065496756275649338348884592698294163140147050914141795\n"}︡
︠fe7edfdd-21c9-4747-ae69-d8541d25e618︠
RR
︡5928a973-3dee-4f28-94ad-88d80d7c6e27︡{"stdout":"Real Field with 53 bits of precision\n"}︡
︠cb2288ab-9011-40b1-b1e2-307b5e270e7a︠
RR.pi()
︡18dc2169-4f1a-498a-b817-4c9d412928a4︡{"stdout":"3.14159265358979\n"}︡
︠58d83865-e4ea-4c41-8bf0-63eb53079620︠
RF = RealField(156)
︡385d6fbd-05d3-4b56-acbb-4757039369e2︡
︠ab5dc66f-6162-4044-ac98-833fd867e482︠
RF
︡e5ab55a3-4d9c-4cba-9c45-2d7670a4c623︡{"stdout":"Real Field with 156 bits of precision\n"}︡
︠5c8fc59f-68f7-4684-aeff-e6aaba407827︠
RF.pi()
︡04398e6e-25f6-4572-902c-62b85f8d1f4c︡{"stdout":"3.141592653589793238462643383279502884197169399\n"}︡
︠92766ed0-426d-41fb-b44c-7272367a8aa7︠









