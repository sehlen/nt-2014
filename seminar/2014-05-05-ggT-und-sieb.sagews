︠2a7a276f-b711-47ee-bd92-67c0be09ecffi︠
%md
#### Wir implementieren den ggT so elementar wie möglich

Dazu beginnen wir mit der Imlementation der Division mit Rest
︡4a28a84a-f761-4120-b4db-f0256abe9693︡{"html":"<h4>Wir implementieren den ggT so elementar wie möglich</h4>\n\n<p>Dazu beginnen wir mit der Imlementation der Division mit Rest</p>\n"}︡
︠b0027643-462b-4fdb-9be6-43671f3e608f︠
def division_with_remainder(a,b):
    r"""
      We implemented Algorithm 1.1.12
      in William Stein's book
      Elementary Number Theory:
      Primes, Congruences, and Secrets
      (although the actual algorithm is not stated there)
    """
    assert a in ZZ
    assert b in ZZ
    if b == 0:
        raise ValueError("b = 0 is not allowed")
    if a == 0:
        return (0, 0)
    q = 0
    r = a
    s = sign(a)*sign(b)
    while r >= abs(b) or r < 0:
        r = r - s*b
        #print r
        q = q+1
    return s*q, r
︡5e5c381e-7ef1-4364-be77-6bdc5b577215︡
︠b7e79680-f4d1-4dfd-8f01-c907442b0a43i︠
%md
#### Zunächst testen wir die Implementation mal etwas
︡63edc279-5312-4f75-9ce5-065e76f6dd93︡{"html":"<h4>Zunächst testen wir die Implementation mal etwas</h4>\n"}︡
︠5f429f77-4700-47b9-9383-113db146e6aa︠
def test_div(a,b):
    q,r = division_with_remainder(a,b)
    if not q*b+r == a:
        return False
    else:
        return True
︡f8fccb5f-66e6-490a-b136-10d8f9b89440︡
︠a5d2e0b3-fbe8-491e-ac23-8665f9dd36d1︠
test_div(-10,-9)
︡72deb473-ed19-43ab-b712-4d7792ae1e65︡{"stdout":"True\n"}︡
︠9f9d0d83-afdc-4f21-ac2c-23752d81df15︠
def test_div_range(M,N):
    r"""
      Test the division algorithm in the range M,N,
      that is, for all pairs (a,b) with M <= a <= N and M <= b <= N.
    """
    for a in range(M,N+1):
        for b in range(M,N+1):
            if not b==0 and not test_div(a,b):
                raise RuntimeError("There was an error for a = {0}, b = {1}".format(a,b))
    print "Test passed."
︡973c5432-0867-462d-b7cc-770a9f98e06c︡
︠bcebf3dc-e0db-4553-ba5a-6019385f3d03︠
test_div_range(-100,100)
︡4a14c4f4-b54b-4d71-8e65-b0026a7d5992︡{"stdout":"Test passed."}︡{"stdout":"\n"}︡
︠b5a4f83b-b363-4d74-a6d6-8562ce655603︠
def ggT(a,b):
    r"""
      We implemented Algorithm 1.1.13
      in William Stein's book
      Elementary Number Theory:
      Primes, Congruences, and Secrets
    """
    a = abs(a)
    b = abs(b)
    if a == b:
        return a
    if not a > b:
        a,b = b,a
    if b == 0:
        return a
    q = 0
    r = a
    while True:
        q,r = division_with_remainder(a,b)
        if r == 0:
            return b
        a = b
        b = r
︡df3d02ea-d118-47f5-b83b-26f808da4be8︡
︠26bb0c1d-bf53-4279-89c9-1d530d94536a︠
ggT(2*3^2*5*7*11,3*11^2*101)
︡6c810c39-fe8e-4ac8-a7cc-3f1758b8a410︡{"stdout":"33\n"}︡
︠dba51e79-b399-4570-aad2-edfefd858a10i︠
%md
#### Und wieder: Testen des ggT gegen die Sage-Implementation gcd
Testen ist immer sehr wichtig beim Programmieren!
︡4e7075e9-4d74-485d-8a6f-feac7387e695︡{"html":"<h4>Und wieder: Testen des ggT gegen die Sage-Implementation gcd</h4>\n\n<p>Testen ist immer sehr wichtig beim Programmieren!</p>\n"}︡
︠08279533-b19b-482a-8406-8490a2a52cf3︠
def test_ggT(M,N):
    for a in range(M,N+1):
        for b in range(M,N+1):
            g = ggT(a,b)
            G = gcd(a,b)
            if g != G:
                raise RuntimeError("There was an error for a = {0}, b = {1}. ggT({0},{1}) returned {2} and gcd({0},{1}) returned {3}".format(a,b,g,G))
    print "Test passed."
︡80206fc3-bfbf-454c-9805-b45610688f92︡
︠2fa2d423-3a60-4b69-a8a9-06d981425bde︠
test_ggT(-50,50)
︡36242ba5-eae4-4d23-af8a-bd751e2a327f︡{"stdout":"Test passed."}︡{"stdout":"\n"}︡
︠f62c220d-af3a-4c16-87fb-021c822aba63i︠
%md
#### Ok, jetzt mal ein kleiner Zeitvergleich
︡a5d78755-cab1-4f6d-8471-fa35e8cf00c4︡{"html":"<h4>Ok, jetzt mal ein kleiner Zeitvergleich</h4>\n"}︡
︠ac02ed70-7318-4460-8e42-93a5a1e59aa2︠
a = ZZ.random_element(10^11)
b = ZZ.random_element(10^5)
%timeit ggT(a,b)
%timeit gcd(a,b)
︡7f90d734-2f32-4d1a-a4e1-26c220bf2a03︡{"stdout":"5 loops, best of 3: 126 ms per loop"}︡{"stdout":"\n"}︡{"stdout":"625 loops, best of 3: 3.84 µs per loop\n"}︡
︠27a4c2bf-8074-454f-b81b-1f4d227314abi︠
%md
#### Wir sehen, ...
...dass unser Algorithmus um einiges langsamer ist.
Diejenigen von Euch, die Arithmetik modulo m benutzt haben (z.B. mittels % oder Zmod),
haben eine deutlich schnellere Imlementation. Aber Eure Implementation benutzt dann schon optimierte "Funktionen" bzw. eher Operationen.
︡ed8c613f-de2a-407d-84ac-36c4f0859fc4︡{"html":"<h4>Wir sehen, &#8230;</h4>\n\n<p>&#8230;dass unser Algorithmus um einiges langsamer ist.\nDiejenigen von Euch, die Arithmetik modulo m benutzt haben (z.B. mittels % oder Zmod),\nhaben eine deutlich schnellere Imlementation. Aber Eure Implementation benutzt dann schon optimierte &#8220;Funktionen&#8221; bzw. eher Operationen.</p>\n"}︡
︠b63888b6-3939-4a00-bc59-a560bb3e996bi︠
%md
#### Jetzt implementieren wir das Sieb des Eratosthenes.
︡f75ac738-cdd8-4078-83f9-46fa9e5c37c6︡{"html":"<h4>Jetzt implementieren wir das Sieb des Eratosthenes.</h4>\n"}︡
︠568db088-78c5-424e-b322-d9f3379d809a︠
def sieb(M, ggT = ggT):
    r"""
      Return a list of all primes <= M.
      By default, we use our own implementation of the gcd "ggT".
      We can change this by passing a different function to the parameter ggT.
    """
    l = range(2,M+1)
    primes = [2]
    i = 0
    while True:
        p = primes[i]
        #print i, p
        if l.count(p^2) > 0:
            R = l.index(p^2)
            if l.count(p) > 0:
                primes += l[l.index(p)+1:R]
            else:
                primes += l[:R]
            i += 1
            #print primes
        else:
            primes = primes + l
            break
        l = filter(lambda x: ggT(p,x) == 1, l[R:])
        #print p, l
    return primes
︡2ba1f814-87f9-4179-a773-342f125acce1︡
︠2c09b220-e0b0-4769-8996-c1348bb3c978︠
sieb(100)
︡c3333216-20ad-48f2-90d7-78cfb16f15ef︡{"stdout":"[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]\n"}︡
︠ffd288b4-fdfd-427a-9995-d7777220f4af︠
sieb(100) == prime_range(100)
︡b943ff64-1343-4894-8bca-978dbb481e7a︡{"stdout":"True\n"}︡
︠e83e34d8-1e15-4742-b057-3d1b73414019︠
%time sieb(1000)
︡e1212add-d299-4881-bbe2-239b943e545c︡{"stdout":"[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]"}︡{"stdout":"\n"}︡{"stdout":"CPU time: 1.57 s, Wall time: 1.57 s\n"}︡
︠004c2b83-8aa0-481b-b033-79ac60d3d999︠
sieb(1000) == prime_range(1000)
︡53778cf4-8894-4fcb-a921-0354c9784889︡{"stdout":"True"}︡{"stdout":"\n"}︡
︠9d836093-156e-415d-883b-b4809fe12458︠
%time sieb(1000, ggT = gcd)
︡e9896835-5bee-41c4-8462-919136f56cbc︡{"stdout":"[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]\n"}︡{"stdout":"CPU time: 0.02 s, Wall time: 0.02 s\n"}︡
︠7867cc57-5ae0-4d17-8772-6dadaa1b6792i︠
%md
Wir sehen, dass die ggT-Implementation hier einiges ausmacht!
︡da8f2508-e303-451e-87a2-3d7fbd46e124︡{"html":"<p>Wir sehen, dass die ggT-Implementation hier einiges ausmacht!</p>\n"}︡
︠9458c6f9-0a6b-4598-9a52-173027e6b676︠
%time A=sieb(100000, ggT = gcd)
︡9a34d3e0-ad19-4151-b7e0-cf10212bb52b︡{"stdout":"CPU time: 7.16 s, Wall time: 7.12 s"}︡{"stdout":"\n"}︡
︠7f86b041-6d6d-4f1b-9454-ff1a36b2f03ei︠
%md
#### Noch ein paar Worte zur Implementation

Ich habe hier die Funktion 'filter' benutzt.

Das erste Argument ist eine Bedingung, die wahr oder falsch sein kann.

Das zweite Argument ist eine Liste.

Zurückgegeben werden die Elemente der Liste, die die Bedingung erfüllen (wahr).

Außerdem habe ich die "slice"-Notation verwendet: l[A:B] gibt nur die Elemente mit Index zwischen A und B zurück.
(Inklusive Index A und ohne Index B)

Lässt mein eines der Argumente weg, heißt dies vom Anfang bzw. bis zum Ende.

Beispiel:
︡aa0dffc7-2b35-4401-ad15-4d889352665b︡{"html":"<h4>Noch ein paar Worte zur Implementation</h4>\n\n<p>Ich habe hier die Funktion &#8216;filter&#8217; benutzt.</p>\n\n<p>Das erste Argument ist eine Bedingung, die wahr oder falsch sein kann.</p>\n\n<p>Das zweite Argument ist eine Liste.</p>\n\n<p>Zurückgegeben werden die Elemente der Liste, die die Bedingung erfüllen (wahr).</p>\n\n<p>Außerdem habe ich die &#8220;slice\"-Notation verwendet: l[A:B] gibt nur die Elemente mit Index zwischen A und B zurück.\n(Inklusive Index A und ohne Index B)</p>\n\n<p>Lässt mein eines der Argumente weg, heißt dies vom Anfang bzw. bis zum Ende.</p>\n\n<p>Beispiel:</p>\n"}︡
︠0bc75252-5ead-4aae-9e35-8d411778fc8b︠
l = ['a', 'b', 'c', 'd']
︡1668967b-ffaf-4f0b-8a24-ba6afef5cc8d︡
︠c53f50d8-85d8-4387-802a-f88952173940︠
l[1:2]
︡fb5dbffe-277f-40fd-b02e-0ad54a94f1cb︡{"stdout":"['b']\n"}︡
︠340d6ad6-f2f5-4298-97f3-810ec63a442d︠
l[:2]
︡45eddfab-e43d-4f5b-aec9-6d01cb342748︡{"stdout":"['a', 'b']\n"}︡
︠08b0a01d-ea70-4eda-8527-c45c064a4787︠
l[2:]
︡46e53ed3-760f-4550-adc3-22098424cff0︡{"stdout":"['c', 'd']\n"}︡
︠33084935-4084-49fb-821c-be44d8277c24︠









