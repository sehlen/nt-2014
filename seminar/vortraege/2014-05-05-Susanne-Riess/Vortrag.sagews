︠0e77d810-33cc-4d2d-821a-5a0ce2885f89i︠
@interact
def f(a=0, b=50, PNT=False, Gauss=False):
    x = var('x')
    g = prime_pi.plot(a,b)
    if PNT:
        g += plot(x/(log(x)-1), (max(a,5),b), color='red')
    if Gauss:
        g += plot(Li, (max(a,2),b), color='darkgreen')
    show(g, gridlines=True,  frame=True, figsize=[10,3])
︡7f8c769b-d349-4ed1-9477-dfb5758d8405︡{"interact":{"style":"None","flicker":false,"layout":[[["a",12,null]],[["b",12,null]],[["PNT",12,null]],[["Gauss",12,null]],[["",12,null]]],"id":"fa9834fc-61bf-4c83-bc5d-dc7662326c21","controls":[{"control_type":"input-box","default":0,"label":"a","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"a","type":null},{"control_type":"input-box","default":50,"label":"b","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"b","type":null},{"default":false,"var":"PNT","readonly":false,"control_type":"checkbox","label":"PNT"},{"default":false,"var":"Gauss","readonly":false,"control_type":"checkbox","label":"Gauss"}]}}︡
︠1a77d635-5db9-40a7-b702-3e2bb56c5d8e︠









