︠4d208fd0-d0fa-4a78-8828-c2dce9b68890i︠
%md
# Die Riemannsche Vermutung (RH)

Die Riemannsche Vermutung ist eines der 7 Milleniumsprobleme und postuliert eine sehr präzise Antwort auf die Frage wie die Primzahlen verteilt sind. Sie wurde 1859 von Bernhard Riemann formuliert und ist seither weder bewiesen noch widerlegt worden.


## Eine erste Formulierung der RH

Alle nicht-trivialen Nullstellen der Riemannschen Zetafunktion haben den Realteil $\frac{1}{2}$.


## Die Riemannsche Zetafunktion

Die Riemannsche Zetafunktion $\zeta(s)$ ist für komplexe Zahlen $s\in\mathbb{C}$ mit $Re(s)>1$ über die Dirichletreihe definiert:
$\zeta(s)=\sum_{n=1}^{\infty} \frac{1}{n^s}$

**Satz:**  $\zeta(s)=\sum_{n=1}^{\infty} \frac{1}{n^s}$ konvergiert gleichmäßig und absolut für $Re(s)>1$ und es gilt die Euler Identität: $\zeta(s)=\prod_{p \mbox{ } prim} \frac{1}{1-p^{-s}}$.

Die Euler Identität drückt also das Gesetz der eindeutigen Primfaktorzerlegung in einer einzigen Gleichung aus und stellt einen ersten Zusammenhang zwischen der Riemannschen Zetafunktion und Primzahlen her.

**Satz:** $\zeta(s)$ hat eine analytische Fortsetzung auf $\mathbb{C}$ \ {1} und einen einfachen Pol bei $s=1$.

Um dies zu zeigen benötigt man die Gammafunktion, die vollständige Zetafunktion, die Jacobische Thetafunktion und das sogenannte Mellinprinzip mit der Mellintransformierten.

Die Gammafunktion ist definiert als $\Gamma(s)=\int_{0}^{\infty}e^{-y}y^s\frac{dy}{y}$ für $s\in\mathbb{C}$ mit $Re(s)> 0$. Sie ist überall ungleich null und hat nur einfache Pole. Diese sind genau die negativen ganzen Zahlen und null. Außerdem ist sie eine analytische Funktion und besitzt eine meromorphe Fortsetzung auf ganz $\mathbb{C}$.

Damit gilt dann $\pi^{-\frac{s}{2}}\Gamma(\frac{s}{2})\zeta{s}=\pi^{\frac{s-1}{2}}\Gamma(\frac{1-s}{2})\zeta{1-s}$.


### Spezielle Funktionswerte der Riemannschen Zetafunktion

Einige Funktionswerte sind durch die sogenannten Bernoulli-Zahlen $B_k$ gegeben. Diese sind definiert als Koeffizienten der Reihenentwicklung der Funktion:
$F(t)=\frac{te^t}{e^t-1}=\sum_{k=0}^{\infty} B_k \frac{t^k}{k!}$

**Satz:** Für alle natürlichen Zahlen $k$ gilt:
$\zeta(1-k)=-\frac{B_k}{k}$ und $\zeta(2k)=(-1)^{k-1} \frac{2\pi^{2k}}{2(2k)!}B_{2k}$


### Nullstellen der Riemannschen Zetafunktion
Mit der Euler Identität folgt, dass $\zeta(s)\neq 0$ für alle $s\in\mathbb{C}$ mit $Re(s)>1$.
Aus

- $\pi^{-\frac{s}{2}}\Gamma(\frac{s}{2})\zeta(s)=\pi^{\frac{s-1}{2}}\Gamma(\frac{1-s}{2})\zeta(1-s)$
- $\Gamma(s)\neq 0$
- Die einzigen Pole von $\Gamma(s)$ sind einfach und an den Stellen $-s\in \mathbb{N}_0$
- Gilt $Re(s)< 0$ so gilt $Re(1-s)>1$

folgt, dass für $Re(s)< 0$ die einzigen Nullstellen bei -2,-4,-6,... liegen. Diese nennen wir triviale Nullstellen von $\zeta(s)$.

Für alle anderen Nullstellen $s\in\mathbb{C}$ gilt also $Re(s)\in[0,1]$. Diesen Bereich nennt man auch kritischen Streifen.

Die Riemannsche Vermutung postuliert nun, dass für diese sogar gilt $Re(s)=\frac{1}{2}$.


## Zusammenhang Riemannsche Vermutung, $\zeta(s)$, Primzahlen und $\pi(x)$

$\pi(x) = |\{n \leq x\ \mid\ n \text{ ist Primzahl}\}|$

**Primzahlsatz:** Es gilt $\pi(x) \sim \frac{x}{\ln(x)}$.

Approximationen von $\pi(x)$ sind gegeben durch:

- $\frac{x}{\ln(x)-1}$
- $Li(x)=\int_{2}^{x} \frac{1}{\ln(t)} dt$



︡5f8a6822-9654-4ef3-b019-900e3b2e2fff︡{"html":"<h1>Die Riemannsche Vermutung (RH)</h1>\n\n<p>Die Riemannsche Vermutung ist eines der 7 Milleniumsprobleme und postuliert eine sehr präzise Antwort auf die Frage wie die Primzahlen verteilt sind. Sie wurde 1859 von Bernhard Riemann formuliert und ist seither weder bewiesen noch widerlegt worden.</p>\n\n<h2>Eine erste Formulierung der RH</h2>\n\n<p>Alle nicht-trivialen Nullstellen der Riemannschen Zetafunktion haben den Realteil $\\frac{1}{2}$.</p>\n\n<h2>Die Riemannsche Zetafunktion</h2>\n\n<p>Die Riemannsche Zetafunktion $\\zeta(s)$ ist für komplexe Zahlen $s\\in\\mathbb{C}$ mit $Re(s)>1$ über die Dirichletreihe definiert:\n$\\zeta(s)=\\sum_{n=1}^{\\infty} \\frac{1}{n^s}$</p>\n\n<p><strong>Satz:</strong>  $\\zeta(s)=\\sum_{n=1}^{\\infty} \\frac{1}{n^s}$ konvergiert gleichmäßig und absolut für $Re(s)>1$ und es gilt die Euler Identität: $\\zeta(s)=\\prod_{p \\mbox{ } prim} \\frac{1}{1-p^{-s}}$.</p>\n\n<p>Die Euler Identität drückt also das Gesetz der eindeutigen Primfaktorzerlegung in einer einzigen Gleichung aus und stellt einen ersten Zusammenhang zwischen der Riemannschen Zetafunktion und Primzahlen her.</p>\n\n<p><strong>Satz:</strong> $\\zeta(s)$ hat eine analytische Fortsetzung auf $\\mathbb{C}$ \\ {1} und einen einfachen Pol bei $s=1$.</p>\n\n<p>Um dies zu zeigen benötigt man die Gammafunktion, die vollständige Zetafunktion, die Jacobische Thetafunktion und das sogenannte Mellinprinzip mit der Mellintransformierten.</p>\n\n<p>Die Gammafunktion ist definiert als $\\Gamma(s)=\\int_{0}^{\\infty}e^{-y}y^s\\frac{dy}{y}$ für $s\\in\\mathbb{C}$ mit $Re(s)> 0$. Sie ist überall ungleich null und hat nur einfache Pole. Diese sind genau die negativen ganzen Zahlen und null. Außerdem ist sie eine analytische Funktion und besitzt eine meromorphe Fortsetzung auf ganz $\\mathbb{C}$.</p>\n\n<p>Damit gilt dann $\\pi^{-\\frac{s}{2}}\\Gamma(\\frac{s}{2})\\zeta{s}=\\pi^{\\frac{s-1}{2}}\\Gamma(\\frac{1-s}{2})\\zeta{1-s}$.</p>\n\n<h3>Spezielle Funktionswerte der Riemannschen Zetafunktion</h3>\n\n<p>Einige Funktionswerte sind durch die sogenannten Bernoulli-Zahlen $B_k$ gegeben. Diese sind definiert als Koeffizienten der Reihenentwicklung der Funktion:\n$F(t)=\\frac{te^t}{e^t-1}=\\sum_{k=0}^{\\infty} B_k \\frac{t^k}{k!}$</p>\n\n<p><strong>Satz:</strong> Für alle natürlichen Zahlen $k$ gilt: \n$\\zeta(1-k)=-\\frac{B_k}{k}$ und $\\zeta(2k)=(-1)^{k-1} \\frac{2\\pi^{2k}}{2(2k)!}B_{2k}$</p>\n\n<h3>Nullstellen der Riemannschen Zetafunktion</h3>\n\n<p>Mit der Euler Identität folgt, dass $\\zeta(s)\\neq 0$ für alle $s\\in\\mathbb{C}$ mit $Re(s)>1$.\nAus </p>\n\n<ul>\n<li>$\\pi^{-\\frac{s}{2}}\\Gamma(\\frac{s}{2})\\zeta(s)=\\pi^{\\frac{s-1}{2}}\\Gamma(\\frac{1-s}{2})\\zeta(1-s)$</li>\n<li>$\\Gamma(s)\\neq 0$</li>\n<li>Die einzigen Pole von $\\Gamma(s)$ sind einfach und an den Stellen $-s\\in \\mathbb{N}_0$ </li>\n<li>Gilt $Re(s)< 0$ so gilt $Re(1-s)>1$ </li>\n</ul>\n\n<p>folgt, dass für $Re(s)< 0$ die einzigen Nullstellen bei -2,-4,-6,&#8230; liegen. Diese nennen wir triviale Nullstellen von $\\zeta(s)$.</p>\n\n<p>Für alle anderen Nullstellen $s\\in\\mathbb{C}$ gilt also $Re(s)\\in[0,1]$. Diesen Bereich nennt man auch kritischen Streifen.</p>\n\n<p>Die Riemannsche Vermutung postuliert nun, dass für diese sogar gilt $Re(s)=\\frac{1}{2}$.</p>\n\n<h2>Zusammenhang Riemannsche Vermutung, $\\zeta(s)$, Primzahlen und $\\pi(x)$</h2>\n\n<p>$\\pi(x) = |\\{n \\leq x\\ \\mid\\ n \\text{ ist Primzahl}\\}|$</p>\n\n<p><strong>Primzahlsatz:</strong> Es gilt $\\pi(x) \\sim \\frac{x}{\\ln(x)}$.</p>\n\n<p>Approximationen von $\\pi(x)$ sind gegeben durch:</p>\n\n<ul>\n<li>$\\frac{x}{\\ln(x)-1}$</li>\n<li>$Li(x)=\\int_{2}^{x} \\frac{1}{\\ln(t)} dt$</li>\n</ul>\n"}︡
︠7c3e6097-fca3-4e9c-841e-462e43427944︠
@interact
def f(a=0, b=50, PNT=False, Gauss=False):
    x = var('x')
    g = prime_pi.plot(a,b)
    if PNT:
        g += plot(x/(log(x)-1), (max(a,5),b), color='red')
    if Gauss:
        g += plot(Li, (max(a,2),b), color='darkgreen')
    show(g, gridlines=True,  frame=True, figsize=[10,3])
︡a1837efb-49be-4bb8-b7cf-ec5e3efc73c7︡{"interact":{"style":"None","flicker":false,"layout":[[["a",12,null]],[["b",12,null]],[["PNT",12,null]],[["Gauss",12,null]],[["",12,null]]],"id":"111f3172-df3d-468f-a55f-1143f3131e32","controls":[{"control_type":"input-box","default":0,"label":"a","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"a","type":null},{"control_type":"input-box","default":50,"label":"b","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"b","type":null},{"default":false,"var":"PNT","readonly":false,"control_type":"checkbox","label":"PNT"},{"default":false,"var":"Gauss","readonly":false,"control_type":"checkbox","label":"Gauss"}]}}︡
︠0eab0236-9c67-4456-bd26-c2f7e2072e9ci︠
%md
## Äquivalente Formulierungen der Riemannschen Vermutung

Äquivalent zur Riemannschen Vermutung ist folgendes: Es existiert eine Konstante $c$, sodass für alle $x$ gilt $|\pi(x)-Li(x)|\leq c \sqrt{x} \ln(x)$.

Fügt man der $\pi(x)$-Funktion weitere Stufen an der Stelle $x=1$ mit Höhe $log(2\pi)$ und an den Stellen $x=p^n$ mit Höhe $log(p)$ hinzu, so ergibt sich dadurch eine Funktion die der Funktion $f(x)=x$ ähnelt.

Daraus ergibt sich eine weitere Formulierung der Riemannschen Vermutung:
Die neu konstruierte Funktion ist im wesentlichen Quadratwurzel exakt zu $f(x)=x$. Dies bedeutet, dass der Betrag der Differenz kleiner als $x^l$ für alle $l>0,5$ ist.


## Riemanns exakte Formel für $\pi(x)$

Definiere
$R(x)=Li(x)-\frac{1}{2}Li(\sqrt{x}-\frac{1}{3}Li(\sqrt[3]{x})....$
wobei das Vorzeichen von $\frac{1}{n}Li(\sqrt[n]{x})$ definiert ist durch:

- minus, wenn $n$ das Produkt einer ungeraden Anzahl von verschiedenen Primzahlen ist
- positiv, wenn $n$ das Produkt einer geraden Anzahl von verschiedenen Primzahlen ist
- 0, wenn die Primfaktorzerlegung von $n$ eine Primzahlpotenz enthält.

Äquivalent kann man $R(x)$ auch ausdrücken durch:
$R(x)=1+\sum_{n=1}{\infty}\frac{1}{n\zeta(n+1}\frac{(\ln(x))^n}{n!}$

Damit ergibt sich folgende exakte Formel für $\pi(x)$:
$\pi(x)=R(x)-\sum_{p}R(x^p)$
wobei $p$ hier eine Nullstelle von $\zeta(s)$ im kritischen Streifen ist.

Die k-te Approximation ist dann $R_k(x)=R(x)+T_1(x)+\dots+T_k(x)$, wobei $T_n(x)=-R(x^{p_n })-R(x^{\overline{p_n}})$ der Beitrag des n-ten Wurzelpaares von $\zeta(s)$ ist.

Für eine Annimation der Approximation von $\pi (x)$ mit den ersten 500 Wurzeln und für eine Annimation der Approximation von $\pi (x)$ im Intervall $190 \leq x \le 230$ mit den ersten 500 Nullstellen:

![Anim1](http://www.dartmouth.edu/~chance/chance_news/for_chance_news/Riemann/piCloseAnim500.gif)

![Anim2](http://www.dartmouth.edu/~chance/chance_news/for_chance_news/Riemann/piApproxAnimX2.gif)


## Verallgemeinerung der Riemannschen Vermutung

Die verallgemeinerte Riemannsche Vermutung postuliert, dass die analytische Fortsetzung einer Dirichletschen L-Reihe im kritischen Streifen nur Nullstellen mit Realteil $\frac{1}{2}$ hat.

Hierbei ist die Dirichletsche L-Reihe gegeben durch $L(\chi,s)=\sum_{n=1}^{\infty}\frac{\chi(n)}{n^s}$ mit $s\in\mathbb{C}$, sodass $Re(s)> 1$. Dabei ist $\chi$   ein sogenannter Dirichletscher Charakter $\mod m$ und gegeben durch $\chi:(\mathbb{Z}/m\mathbb{Z})\rightarrow S^1=\{z\in\mathbb{C}: |z|=1\}$. Weiterhin ist dann $\chi(n)=\begin{cases}\chi(n\mod m)& \gcd(n,m)=1 \\ 0 & \mbox{sonst} \end{cases}$ für $n,m \in\mathbb{N}$.




## Was spricht für die Riemannsche Vermutung?

- Die Riemannsche Vermutung wurde bislang für etwa $10^{13}$ Nullstellen im kritischen Streifen überprüft und gilt damit für alle Zahlen $s\in\mathbb{C}$ mit $Im(s)< 2,4 \cdot 10^{12}$.
- Es wurde bewiesen, dass der Realteil der Nullstellen im kritischen Streifen nicht auf dessen Rand liegt (also $Re(s)\in(0,1)$).
- Es wurde bewiesen, dass $\zeta(s)$ unendlich viele Nullstellen $s$ mit $Re(s)=\frac{1}{2}$ hat.
- Es wurde bewiesen, dass für mindestens 40% aller nicht-trivialen Nullstellen $Re(s)=\frac{1}{2}$ gilt.
- Das komplex konjugierte einer Nullstelle von $\zeta(s)$ ist ebenfalls eine Nullstelle.


## Was folgt aus der Riemannschen Vermutung (kleiner Auszug)?

- Der Primzahlsatz
- Man kann Schranken für das Wachstum vieler anderer arithmetischer Funktionen geben (z.b. Möbiusfunktion).
- Man kann damit beweisen, dass die Lücke zwischen zwei aufeinander folgenden Primzahlen höchstens $O(\sqrt{p}\log(p))$ ist.
︡8cccdd3c-3a6f-47c2-ab3a-f98e8029198b︡{"html":"<h2>Äquivalente Formulierungen der Riemannschen Vermutung</h2>\n\n<p>Äquivalent zur Riemannschen Vermutung ist folgendes: Es existiert eine Konstante $c$, sodass für alle $x$ gilt $|\\pi(x)-Li(x)|\\leq c \\sqrt{x} \\ln(x)$.</p>\n\n<p>Fügt man der $\\pi(x)$-Funktion weitere Stufen an der Stelle $x=1$ mit Höhe $log(2\\pi)$ und an den Stellen $x=p^n$ mit Höhe $log(p)$ hinzu, so ergibt sich dadurch eine Funktion die der Funktion $f(x)=x$ ähnelt.</p>\n\n<p>Daraus ergibt sich eine weitere Formulierung der Riemannschen Vermutung:\nDie neu konstruierte Funktion ist im wesentlichen Quadratwurzel exakt zu $f(x)=x$. Dies bedeutet, dass der Betrag der Differenz kleiner als $x^l$ für alle $l>0,5$ ist.</p>\n\n<h2>Riemanns exakte Formel für $\\pi(x)$</h2>\n\n<p>Definiere\n$R(x)=Li(x)-\\frac{1}{2}Li(\\sqrt{x}-\\frac{1}{3}Li(\\sqrt[3]{x})....$\nwobei das Vorzeichen von $\\frac{1}{n}Li(\\sqrt[n]{x})$ definiert ist durch:</p>\n\n<ul>\n<li>minus, wenn $n$ das Produkt einer ungeraden Anzahl von verschiedenen Primzahlen ist</li>\n<li>positiv, wenn $n$ das Produkt einer geraden Anzahl von verschiedenen Primzahlen ist</li>\n<li>0, wenn die Primfaktorzerlegung von $n$ eine Primzahlpotenz enthält.</li>\n</ul>\n\n<p>Äquivalent kann man $R(x)$ auch ausdrücken durch:\n$R(x)=1+\\sum_{n=1}{\\infty}\\frac{1}{n\\zeta(n+1}\\frac{(\\ln(x))^n}{n!}$</p>\n\n<p>Damit ergibt sich folgende exakte Formel für $\\pi(x)$:\n$\\pi(x)=R(x)-\\sum_{p}R(x^p)$\nwobei $p$ hier eine Nullstelle von $\\zeta(s)$ im kritischen Streifen ist.</p>\n\n<p>Die k-te Approximation ist dann $R_k(x)=R(x)+T_1(x)+\\dots+T_k(x)$, wobei $T_n(x)=-R(x^{p_n })-R(x^{\\overline{p_n}})$ der Beitrag des n-ten Wurzelpaares von $\\zeta(s)$ ist.</p>\n\n<p>Für eine Annimation der Approximation von $\\pi (x)$ mit den ersten 500 Wurzeln und für eine Annimation der Approximation von $\\pi (x)$ im Intervall $190 \\leq x \\le 230$ mit den ersten 500 Nullstellen:</p>\n\n<p><img src=\"http://www.dartmouth.edu/~chance/chance_news/for_chance_news/Riemann/piCloseAnim500.gif\" alt=\"Anim1\" /></p>\n\n<p><img src=\"http://www.dartmouth.edu/~chance/chance_news/for_chance_news/Riemann/piApproxAnimX2.gif\" alt=\"Anim2\" /></p>\n\n<h2>Verallgemeinerung der Riemannschen Vermutung</h2>\n\n<p>Die verallgemeinerte Riemannsche Vermutung postuliert, dass die analytische Fortsetzung einer Dirichletschen L-Reihe im kritischen Streifen nur Nullstellen mit Realteil $\\frac{1}{2}$ hat.</p>\n\n<p>Hierbei ist die Dirichletsche L-Reihe gegeben durch $L(\\chi,s)=\\sum_{n=1}^{\\infty}\\frac{\\chi(n)}{n^s}$ mit $s\\in\\mathbb{C}$, sodass $Re(s)> 1$. Dabei ist $\\chi$   ein sogenannter Dirichletscher Charakter $\\mod m$ und gegeben durch $\\chi:(\\mathbb{Z}/m\\mathbb{Z})\\rightarrow S^1=\\{z\\in\\mathbb{C}: |z|=1\\}$. Weiterhin ist dann $\\chi(n)=\\begin{cases}\\chi(n\\mod m)& \\gcd(n,m)=1 \\\\ 0 & \\mbox{sonst} \\end{cases}$ für $n,m \\in\\mathbb{N}$.</p>\n\n<h2>Was spricht für die Riemannsche Vermutung?</h2>\n\n<ul>\n<li>Die Riemannsche Vermutung wurde bislang für etwa $10^{13}$ Nullstellen im kritischen Streifen überprüft und gilt damit für alle Zahlen $s\\in\\mathbb{C}$ mit $Im(s)< 2,4 \\cdot 10^{12}$.</li>\n<li>Es wurde bewiesen, dass der Realteil der Nullstellen im kritischen Streifen nicht auf dessen Rand liegt (also $Re(s)\\in(0,1)$).</li>\n<li>Es wurde bewiesen, dass $\\zeta(s)$ unendlich viele Nullstellen $s$ mit $Re(s)=\\frac{1}{2}$ hat.</li>\n<li>Es wurde bewiesen, dass für mindestens 40% aller nicht-trivialen Nullstellen $Re(s)=\\frac{1}{2}$ gilt.</li>\n<li>Das komplex konjugierte einer Nullstelle von $\\zeta(s)$ ist ebenfalls eine Nullstelle.</li>\n</ul>\n\n<h2>Was folgt aus der Riemannschen Vermutung (kleiner Auszug)?</h2>\n\n<ul>\n<li>Der Primzahlsatz</li>\n<li>Man kann Schranken für das Wachstum vieler anderer arithmetischer Funktionen geben (z.b. Möbiusfunktion).</li>\n<li>Man kann damit beweisen, dass die Lücke zwischen zwei aufeinander folgenden Primzahlen höchstens $O(\\sqrt{p}\\log(p))$ ist.</li>\n</ul>\n"}︡
︠584a004b-6bea-47a0-8c7b-04636d450ed0︠









