︠4153f006-3487-45d4-a627-3e96d72e472ei︠
%md
## Faktorisierungsalgorithmen

Bei großen Zahlen ist Ausprobieren von Teilern nicht sehr effektiv. Daher braucht man andere Vorgehensweisen um echte Teiler großer Zahlen zu finden.

1. Algorithmus: Pollards p-1 Mathode (ohne elliptische Kurven)

2. Algorithmus: Lenstras Faktorisierungsalgorithmus (mit elliptischen Kurven)

︡5af04c0e-8e50-4c77-a13c-7de885690d85︡{"html":"<h2>Faktorisierungsalgorithmen</h2>\n\n<p>Bei großen Zahlen ist Ausprobieren von Teilern nicht sehr effektiv. Daher braucht man andere Vorgehensweisen um echte Teiler großer Zahlen zu finden.</p>\n\n<ol>\n<li><p>Algorithmus: Pollards p-1 Mathode (ohne elliptische Kurven)</p></li>\n<li><p>Algorithmus: Lenstras Faktorisierungsalgorithmus (mit elliptischen Kurven)</p></li>\n</ol>\n"}︡
︠82b8de03-f35f-47bf-a4fe-916ce6d302d7i︠
%md
## Definition
$n,B \in \mathbb{N}$ mit $n=\prod_{i=1}^r p_i^{e_i}$ als Primfaktorzerlegung (also $p_i$ verschiedene Primzahlen).

Dann ist:

n B-potenzglatt $\iff \forall i=1, ..., r: p_i^{e_i} \leq B$

## Beispiele
$100 =2^2 5^2$ ist nicht 20-potenzglatt, da $5^2=25< 20$.

$99=3^2 11$ ist 11-potenzglatt, da $11\leq 11$ und $3^2=9<11$.


︡e6d72223-c093-484b-b9dc-73c34ef96dc3︡{"html":"<h2>Definition</h2>\n\n<p>$n,B \\in \\mathbb{N}$ mit $n=\\prod_{i=1}^r p_i^{e_i}$ als Primfaktorzerlegung (also $p_i$ verschiedene Primzahlen).</p>\n\n<p>Dann ist:</p>\n\n<p>n B-potenzglatt $\\iff \\forall i=1, ..., r: p_i^{e_i} \\leq B$</p>\n\n<h2>Beispiele</h2>\n\n<p>$100 =2^2 5^2$ ist nicht 20-potenzglatt, da $5^2=25< 20$.</p>\n\n<p>$99=3^2 11$ ist 11-potenzglatt, da $11\\leq 11$ und $3^2=9<11$.</p>\n"}︡
︠ead0a085-6b27-4a6a-86ec-7352985649a9i︠
%md
## Pollards (p-1)-Methode

Gegeben ist $N \in \mathbb{N}$ und eine "Suchgrenze" $B \in \mathbb{N}$.

**Ziel:** Finde echten Teiler $1 \neq g \neq N$ von N.

**Idee:**
Unter der Annahme, dass N einen Primteiler p besitzt, für den p-1 B-potenzglatt ist, können wir einen Teiler >1 finden:

1. Wähle a teilerfremd zu p (z.B. a=2, 3, ... dann gilt Teilerfremdheit, da man kleine Primteiler von N ausschließen kann bzw. vorher durch sie teilen kann)

2. Mit kleinem Satz von Fermat gilt dann $a^{p-1} \equiv 1 \mod p$

3. Setze m = kgV(1,...,B)

4. Da p-1 B-potenzglatt ist, ist p-1 ein Teiler von m. Also folgt

$a^m \equiv 1 \mod p$

$\implies a^m-1 \equiv 0 \mod p$

Dann ist p Teiler von N und $a^m-1$, also Teiler von $ggT(N,a^m-1)$.

$\implies g:=ggT(N,a^m-1)\geq p > 1$ und damit ist g>1 Teiler von N.

**Algorithmus:**

1. m=kgV(1,...,B)

2. a=2 (Initialisierung)

3. $x=a^m-1 \mod N$ und $g=ggT(x,N)$

4. falls $1 \neq g \neq N$: fertig, da g echter Teiler

falls $g=1$: obige Argumentation funktioniert nicht, also ex. kein Primteiler p von N mit p-1 B-potenzglatt. Man könnte dann den Algo. mit größerem B versuchen.

falls $g=N$: $ggT(x,N)=N$ also $a^m-1 \equiv 0 \mod q^r$ für alle Primpotenzen $q^r | N$. Man könnte dann ein kleineres B oder anderes a versuchen.


︡d983b3e9-753c-4deb-a1d2-28b1a68cacec︡{"html":"<h2>Pollards (p-1)-Methode</h2>\n\n<p>Gegeben ist $N \\in \\mathbb{N}$ und eine &#8220;Suchgrenze&#8221; $B \\in \\mathbb{N}$.</p>\n\n<p><strong>Ziel:</strong> Finde echten Teiler $1 \\neq g \\neq N$ von N.</p>\n\n<p><strong>Idee:</strong>\nUnter der Annahme, dass N einen Primteiler p besitzt, für den p-1 B-potenzglatt ist, können wir einen Teiler &gt;1 finden:</p>\n\n<ol>\n<li><p>Wähle a teilerfremd zu p (z.B. a=2, 3, &#8230; dann gilt Teilerfremdheit, da man kleine Primteiler von N ausschließen kann bzw. vorher durch sie teilen kann)</p></li>\n<li><p>Mit kleinem Satz von Fermat gilt dann $a^{p-1} \\equiv 1 \\mod p$</p></li>\n<li><p>Setze m = kgV(1,&#8230;,B)</p></li>\n<li><p>Da p-1 B-potenzglatt ist, ist p-1 ein Teiler von m. Also folgt</p></li>\n</ol>\n\n<p>$a^m \\equiv 1 \\mod p$</p>\n\n<p>$\\implies a^m-1 \\equiv 0 \\mod p$</p>\n\n<p>Dann ist p Teiler von N und $a^m-1$, also Teiler von $ggT(N,a^m-1)$.</p>\n\n<p>$\\implies g:=ggT(N,a^m-1)\\geq p > 1$ und damit ist g>1 Teiler von N.</p>\n\n<p><strong>Algorithmus:</strong></p>\n\n<ol>\n<li><p>m=kgV(1,&#8230;,B)</p></li>\n<li><p>a=2 (Initialisierung)</p></li>\n<li><p>$x=a^m-1 \\mod N$ und $g=ggT(x,N)$</p></li>\n<li><p>falls $1 \\neq g \\neq N$: fertig, da g echter Teiler</p></li>\n</ol>\n\n<p>falls $g=1$: obige Argumentation funktioniert nicht, also ex. kein Primteiler p von N mit p-1 B-potenzglatt. Man könnte dann den Algo. mit größerem B versuchen.</p>\n\n<p>falls $g=N$: $ggT(x,N)=N$ also $a^m-1 \\equiv 0 \\mod q^r$ für alle Primpotenzen $q^r | N$. Man könnte dann ein kleineres B oder anderes a versuchen.</p>\n"}︡
︠70b220d2-446c-4a16-ba7d-6e4cff673f06i︠
%md
**Algorithmus zur Berechnung von kgV(1,...,B)**
︡718c3944-8ab1-4800-941f-1a64c3ec324a︡{"html":"<p><strong>Algorithmus zur Berechnung von kgV(1,&#8230;,B)</strong></p>\n"}︡
︠836c82e7-f313-4927-bd8e-63888395ad98︠
def lcm(B):
    # liste mögl. primfaktoren
    P=prime_range(B+1)
    # bestimmung der exponenten (log_p(B)) und bilden des Produkts
    return prod([p^int(math.log(B)/math.log(p)) for p in P])
︡d5e96431-37e8-4c5d-96a2-6e2bb0ef201e︡
︠0d8841ad-47a8-42b8-beca-8884361f030a︠
lcm(5)
︡86439e20-51a4-478a-bec5-334d44fc1358︡{"stdout":"60\n"}︡
︠e9af4903-8fbb-4973-88f6-552b4e5cd4bfi︠
%md
**Implementierung von Pollards (p-1)-Methode**

Hier werden 5 Werte für a probiert.
︡50645c7e-d960-43ae-87a5-baf781e60e63︡{"html":"<p><strong>Implementierung von Pollards (p-1)-Methode</strong></p>\n\n<p>Hier werden 5 Werte für a probiert.</p>\n"}︡
︠c6e66eb5-f8df-495c-9767-2c366bd6a4f7︠
def pollard(N,B):
    m=lcm(B)
    #Initialisierung
    a=2
    while a<=5:
        #Berechne a^m-1 als mögl. vielfaches von p (p|N)
        x=(Mod(a, N)^m-1).lift()
        g=gcd(x,N)
        #falls 1 != g != N, dann ist g gewünschter teiler
        if(g != 1 and g!=N):
            return g
        print a,g
        a=a+1
    return 'kein Teiler gefunden'

# g=1: Dann ex. kein p|N (prim) mit p-1 B-power smooth (wegen kl. Fermat) -> B größer
# g=N: Dann a^m=1 mod q^r (N=prod (q^r)) -> anderes a oder B kleiner
︡a6e5d5b5-336d-40bb-90d5-a5dc55a7ee92︡
︠32a9169f-53c1-46a5-8339-ed4fdb59cea1︠
pollard(5917,5)

︡a3ada8be-fce7-48c1-bc95-a8d68bc8e2b6︡{"stdout":"61\n"}︡
︠78044d8c-4463-4ff4-b9d7-0918a06518f7︠
pollard(779167,5)
︡fa864924-c5a0-4feb-ad8e-3c1a6e531b10︡{"stdout":"2 1\n3 1\n4 1\n5 1\n'kein Teiler gefunden'\n"}︡
︠9450d898-fa3b-41a7-a7cb-aa9070f03229︠
pollard(779167,15)
︡0e7b72a7-6d56-493e-9006-a67fd0190634︡{"stdout":"2003\n"}︡
︠3af3aaa3-3c1a-4058-97d3-b19accef007b︠
pollard(4331,7)
︡58d004df-e186-4b5b-bdcd-3bac4e4689c6︡{"stdout":"2 4331\n3 4331\n4 4331\n5 4331\n'kein Teiler gefunden'\n"}︡
︠f67fcf1a-6a94-45d8-af6f-e85753bb4dde︠
pollard(4331,6)
︡73b4d1e9-df33-46a4-89b2-34fa073c879c︡{"stdout":"61\n"}︡
︠6f0f6943-e75c-4e97-838c-854bf762fbe3︠
pollard(187,15)
︡9149d06f-4464-40be-91e5-70901208626b︡{"stdout":"2 187\n11\n"}︡
︠f333039c-f824-48ec-905b-9f07f918904a︠
pollard(5959,20)
︡1f77e26d-6945-4fcb-8d58-c2b3641fa49a︡{"stdout":"2 1\n3 1\n4 1\n5 1\n'kein Teiler gefunden'\n"}︡
︠77766f2a-c9d5-443a-91a6-0871e66e5392i︠
%md
**In der Praxis:**

- Man wählt B in der Größenordnung $10^6$.
- In $[10^15, 10^15+10000]$ sind etwa 15% der Primzahlen so, dass p-1 $10^6$-potenzglatt gilt.

**Problem:**

Das größte Problem bei Pollard ist, dass oft p-1 nicht B-potenzglatt ist. Allerdings bedeutet ein größeres B viel mehr Rechenaufwand.

**Lösung:**

Wir haben im ersten Beispiel gesehen, dass 100 nicht 20-potenzglatt ist, aber z.B. 99 sogar 11-potenzglatt ist. Eine kleine Veränderung kann hier also viel ändern. Wir haben für Pollard gebraucht, dass p-1 B-potenzglatt ist, da p-1 die Gruppenordnung von $(\mathbb{Z}/p\mathbb{Z})^*$ ist.
Jetzt wollen wir in elliptischen Kurven über endlichen Körpern arbeiten, hier gibt es andere Gruppenordnungen.
︡2fca029d-52ec-43a1-9fec-842aae2fcd39︡{"html":"<p><strong>In der Praxis:</strong></p>\n\n<ul>\n<li>Man wählt B in der Größenordnung $10^6$.</li>\n<li>In $[10^15, 10^15+10000]$ sind etwa 15% der Primzahlen so, dass p-1 $10^6$-potenzglatt gilt.</li>\n</ul>\n\n<p><strong>Problem:</strong></p>\n\n<p>Das größte Problem bei Pollard ist, dass oft p-1 nicht B-potenzglatt ist. Allerdings bedeutet ein größeres B viel mehr Rechenaufwand.</p>\n\n<p><strong>Lösung:</strong></p>\n\n<p>Wir haben im ersten Beispiel gesehen, dass 100 nicht 20-potenzglatt ist, aber z.B. 99 sogar 11-potenzglatt ist. Eine kleine Veränderung kann hier also viel ändern. Wir haben für Pollard gebraucht, dass p-1 B-potenzglatt ist, da p-1 die Gruppenordnung von $(\\mathbb{Z}/p\\mathbb{Z})^*$ ist.\nJetzt wollen wir in elliptischen Kurven über endlichen Körpern arbeiten, hier gibt es andere Gruppenordnungen.</p>\n"}︡
︠68a09ef6-c387-42f9-8016-dfd8bf1fce91i︠
%md
## Lenstras Faktorisierungsalgorithmus

Es gilt der folgende Satz:

**Satz**

p prim.

$|E(\mathbb{Z}/p\mathbb{Z})|=p+1\pm s$ mit $s \leq 2p^{1/2}$.

Statt eines Beweises geben wir eine Veranschaulichung. Der folgende Plot stellt in rot die maximale und minimale Größe der elliptischen Kurve über $\mathbb{Z}/p\mathbb{Z}$ an und in blau sind die Werte $p+1+s$ und $p+1-s$ dargestellt. Dabei werden nur Kurven mit einer Gleichung der Form $y^2=x^3+ax+1$ getestet.
︡179ec619-5300-4dea-bb13-25ca647f2c69︡{"html":"<h2>Lenstras Faktorisierungsalgorithmus</h2>\n\n<p>Es gilt der folgende Satz:</p>\n\n<p><strong>Satz</strong></p>\n\n<p>p prim.</p>\n\n<p>$|E(\\mathbb{Z}/p\\mathbb{Z})|=p+1\\pm s$ mit $s \\leq 2p^{1/2}$.</p>\n\n<p>Statt eines Beweises geben wir eine Veranschaulichung. Der folgende Plot stellt in rot die maximale und minimale Größe der elliptischen Kurve über $\\mathbb{Z}/p\\mathbb{Z}$ an und in blau sind die Werte $p+1+s$ und $p+1-s$ dargestellt. Dabei werden nur Kurven mit einer Gleichung der Form $y^2=x^3+ax+1$ getestet.</p>\n"}︡
︠ee186489-ae0a-4422-805f-db7456c46999︠
def intEC(p):
    y=[]
    for d in range(1,p):
        if gcd(4*d^3+27,p)==1:
            y.append(EllipticCurve(GF(p),[d,1]).cardinality())
    return max(y),min(y)
︡bccfd6b8-a79e-457f-a587-3c30bdd94103︡
︠d969a564-baa2-4688-b54a-c6161f2320bb︠
xCoord = prime_range(100,200)
yCoord =[intEC(p)[0] for p in xCoord]
yCoord2 =[intEC(p)[1] for p in xCoord]
y1 = [p+1+2*sqrt(p) for p in xCoord]
y2 = [p+1-2*sqrt(p) for p in xCoord]
a=list_plot([]);
a+=list_plot(zip(xCoord, yCoord), color='red');
a+=list_plot(zip(xCoord, y1));
a+=list_plot(zip(xCoord, yCoord2),color='red');
a+=list_plot(zip(xCoord, y2));a
︡c5e3f3c0-78a2-4415-ac19-a94cecdcf77d︡{"once":false,"file":{"show":true,"uuid":"a2ce77d4-0448-49e0-81d2-38149179835b","filename":"/projects/a99e0371-019c-465b-9ee7-08aeb4c389e6/.sage/temp/compute5dc1/24064/tmp_vmCD1a.svg"}}︡
︠61757934-ef29-4f90-a24e-d3bae6f2cd6di︠
%md

Man kann sogar zeigen, dass alle Werte aus dem obigen Intervall in verschiedenen Kurven angenommen werden.

Wir wollen das wieder an einem Beispiel illustrieren. Dazu stellen wir die verschiedenen Gruppengrößen für die Kurven $y^2=x^3+ax+1$ mit $a=1,...,58$ in dem Körper $\mathbb{Z}/59\mathbb{Z}$ dar.
︡6615c871-a011-49ae-8e88-9638d09a1488︡{"html":"<p>Man kann sogar zeigen, dass alle Werte aus dem obigen Intervall in verschiedenen Kurven angenommen werden.</p>\n\n<p>Wir wollen das wieder an einem Beispiel illustrieren. Dazu stellen wir die verschiedenen Gruppengrößen für die Kurven $y^2=x^3+ax+1$ mit $a=1,...,58$ in dem Körper $\\mathbb{Z}/59\\mathbb{Z}$ dar.</p>\n"}︡
︠73a5943e-d882-44de-88db-f041151b042c︠
def sizeEC(p):
    y=[]
    for a in range(1,p):
        if gcd(4*a^3+27,p)==1:
            y.append(EllipticCurve(GF(p),[a,1]).cardinality())
    return y
︡4d823297-563b-4afd-9b8c-a8e1ad200fab︡
︠d093afec-8f5b-40cb-b195-2dd646d62d2c︠
list_plot(sizeEC(59))
︡68a6b1e3-1e4e-4a78-b29f-6a5fd9997998︡{"once":false,"file":{"show":true,"uuid":"6c5fbc18-0d14-48be-abeb-42ba8a365488","filename":"/projects/a99e0371-019c-465b-9ee7-08aeb4c389e6/.sage/temp/compute5dc1/15066/tmp_blThrx.svg"}}︡
︠cbb00282-793c-4ad0-aa8a-130d30eee24e︠
min(sizeEC(59))
max(sizeEC(59))
︡06b1252f-5ec4-4ced-b028-5610b06498e5︡{"stdout":"48\n"}︡{"stdout":"75\n"}︡
︠56c521ce-7606-42a5-9f0e-63a951bd1206i︠
%md

Man sieht, dass durch kleine Veränderung von a die Gruppenordnung stark verändert wird.

**Idee von Lenstras Algo.:**

Nehme an, dass $\mathbb{Z}/N\mathbb{Z}$ ein Körper ist. (Damit wir die elliptische Kurve darüber betrachten können.) Wir hoffen, einen Widerspruch zu bekommen.

Betrachte die ell. Kurve $E(\mathbb{Z}/N\mathbb{Z})$ mit der Gleichung $y^2=x^3+ax+1$ für ein zufälliges $a \in \mathbb{Z}/N\mathbb{Z}$ mit $a^3+27 \in (\mathbb{Z}/N\mathbb{Z})^*$. Dann enthält $E(\mathbb{Z}/N\mathbb{Z})$ den Punkt P=(0,1).

Setze m=kgV(1,...,B).

Versuche mP=P+P+...+P zu berechnen. Falls $\mathbb{Z}/N\mathbb{Z}$ kein Körper ist, ist das nicht unbedingt möglich.

Sei p ein Primteiler von N für den $|E(\mathbb{Z}/p\mathbb{Z})|$ B-potenzglatt ist.

Dann ist in $E(\mathbb{Z}/p\mathbb{Z})$ mP = O, denn m ist ein Vielfaches der Gruppenordnung und O ist das neutrale Element.

Dann muss mP auch in $E(\mathbb{Z}/N\mathbb{Z})$ O sein. Falls es einen weiteren Primteiler q von N mit $mP \neq O$ in $E(\mathbb{Z}/q\mathbb{Z})$ gibt, ist das ein Widerspruch. mP kann also in $E(\mathbb{Z}/N\mathbb{Z})$ nicht berechnet werden.

Das kann dadurch passieren, dass das $\lambda$ nicht berechenbar ist, also $2y_1$ oder $x_1-x_2$ nicht invertierbar in $\mathbb{Z}/N\mathbb{Z}$ sind. Dann haben $2y_1$ und N bzw. $x_1-x_2$ und N einen gemeinsamen Teiler >1. Daraus lässt sich ein Teiler >1 von N berechnen.

**Algorithmus:**

1. m=kgV(1,...,B)
2. wähle a zufällig aus $\mathbb{Z}/N\mathbb{Z}$ mit $a^3+27 \in (\mathbb{Z}/N\mathbb{Z})^*$
3. P=(0,1)
3. versuche mP zu berechnen:

falls nicht möglich: dann ist $\lambda$ an einer Stelle nicht berechenbar und man kann daraus einen Teiler berechnen

falls möglich: versuche mit anderem a
︡382f519f-c1ec-4819-bcee-3d2ffac74e5b︡{"html":"<p>Man sieht, dass durch kleine Veränderung von a die Gruppenordnung stark verändert wird.</p>\n\n<p><strong>Idee von Lenstras Algo.:</strong></p>\n\n<p>Nehme an, dass $\\mathbb{Z}/N\\mathbb{Z}$ ein Körper ist. (Damit wir die elliptische Kurve darüber betrachten können.) Wir hoffen, einen Widerspruch zu bekommen.</p>\n\n<p>Betrachte die ell. Kurve $E(\\mathbb{Z}/N\\mathbb{Z})$ mit der Gleichung $y^2=x^3+ax+1$ für ein zufälliges $a \\in \\mathbb{Z}/N\\mathbb{Z}$ mit $a^3+27 \\in (\\mathbb{Z}/N\\mathbb{Z})^*$. Dann enthält $E(\\mathbb{Z}/N\\mathbb{Z})$ den Punkt P=(0,1).</p>\n\n<p>Setze m=kgV(1,&#8230;,B).</p>\n\n<p>Versuche mP=P+P+&#8230;+P zu berechnen. Falls $\\mathbb{Z}/N\\mathbb{Z}$ kein Körper ist, ist das nicht unbedingt möglich.</p>\n\n<p>Sei p ein Primteiler von N für den $|E(\\mathbb{Z}/p\\mathbb{Z})|$ B-potenzglatt ist.</p>\n\n<p>Dann ist in $E(\\mathbb{Z}/p\\mathbb{Z})$ mP = O, denn m ist ein Vielfaches der Gruppenordnung und O ist das neutrale Element.</p>\n\n<p>Dann muss mP auch in $E(\\mathbb{Z}/N\\mathbb{Z})$ O sein. Falls es einen weiteren Primteiler q von N mit $mP \\neq O$ in $E(\\mathbb{Z}/q\\mathbb{Z})$ gibt, ist das ein Widerspruch. mP kann also in $E(\\mathbb{Z}/N\\mathbb{Z})$ nicht berechnet werden.</p>\n\n<p>Das kann dadurch passieren, dass das $\\lambda$ nicht berechenbar ist, also $2y_1$ oder $x_1-x_2$ nicht invertierbar in $\\mathbb{Z}/N\\mathbb{Z}$ sind. Dann haben $2y_1$ und N bzw. $x_1-x_2$ und N einen gemeinsamen Teiler &gt;1. Daraus lässt sich ein Teiler &gt;1 von N berechnen.</p>\n\n<p><strong>Algorithmus:</strong></p>\n\n<ol>\n<li>m=kgV(1,&#8230;,B)</li>\n<li>wähle a zufällig aus $\\mathbb{Z}/N\\mathbb{Z}$ mit $a^3+27 \\in (\\mathbb{Z}/N\\mathbb{Z})^*$</li>\n<li>P=(0,1)</li>\n<li>versuche mP zu berechnen:</li>\n</ol>\n\n<p>falls nicht möglich: dann ist $\\lambda$ an einer Stelle nicht berechenbar und man kann daraus einen Teiler berechnen</p>\n\n<p>falls möglich: versuche mit anderem a</p>\n"}︡
︠b1a810d9-dde7-4e09-9fe2-b55e73a6cbf0i︠
%md
**Implementation Lenstras Faktorisierungsalgorithmus**

Hier werden 10 elliptische Kurven getestet.
︡76f6cd5e-a709-496b-8911-6d92293cba10︡{"html":"<p><strong>Implementation Lenstras Faktorisierungsalgorithmus</strong></p>\n\n<p>Hier werden 10 elliptische Kurven getestet.</p>\n"}︡
︠5886b0a1-443d-42f4-b005-ac9a15636d49︠
def ecf(N,B):
    m=lcm(B)
    # setzen von Z/NZ als Körper
    R=Integers(N)
    R.is_field = lambda :True
    t=0
    while t<10:
        while True:
            # choose random elliptic curve
            a=R.random_element()
            if gcd(4*a^3+27,N)==1:
                break
        try:
            # versuche m*[0,1] zu berechnen. falls das nicht möglich, so ist ein nenner nicht teilerfremd zu N
            m*EllipticCurve([a,1])([0,1])
        except ZeroDivisionError, msg:
            # msg: "Inverse of <int> does not exist"; man will also "3. Teil" von msg haben
            return gcd(Integer(str(msg).split()[2]),N)
        t=t+1
    return 'kein Teiler gefunden'
︡3360ef9c-4a21-42c9-807f-f3c7e06347fd︡
︠34e0eca1-a1df-4ac2-95e8-bc1b7729efbd︠
ecf(5959,20)
︡72e20d85-e24a-4cc0-b5f9-2e82ad74c635︡{"stdout":"59\n"}︡
︠ee3c4717-38fb-48d2-b01b-46d595341d38︠
ecf(12344577654322367, 200)
︡f925bf01-c268-46d6-a9f3-43e450391684︡{"stdout":"1747795169\n"}︡
︠65979751-998f-4026-adc1-40417ac77e66︠
ecf(3242346657978985645658657,1000)
︡4ae13136-a50f-4a3f-969b-4072ac0b0726︡{"stdout":"2591"}︡{"stdout":"\n"}︡
︠bc95de60-90a1-4d50-be2b-27090d56c2cd︠
ecf(123456397344345643547,1000)
︡cad9e6d3-cf0a-4406-9975-a73cb9d172cd︡{"stdout":"17\n"}︡
︠8255c900-cf42-494e-9375-edc5776428b9i︠
%md

Durch den Übergang zu elliptischen Kurven kommen wir mit einem kleineren B aus und haben dadurch eine höhere Effizienz.
︡e752c1f4-5525-4aff-bcbc-fcf09093015e︡{"html":"<p>Durch den Übergang zu elliptischen Kurven kommen wir mit einem kleineren B aus und haben dadurch eine höhere Effizienz.</p>\n"}︡
︠a08e7ee7-763c-4777-9344-e5a0243b4907i︠
%md

## Literatur

- Elementary Number Theory: Primes, Congruences and Secrets, W. Stein, 2011
- The Arithmetic of Elliptic Curves, J. Silverman, 2009
︡ee9325d5-51ce-4405-b482-ee1b9e19cd22︡{"html":"<h2>Literatur</h2>\n\n<ul>\n<li>Elementary Number Theory: Primes, Congruences and Secrets, W. Stein, 2011</li>\n<li>The Arithmetic of Elliptic Curves, J. Silverman, 2009</li>\n</ul>\n"}︡
︠9430cfe4-f6a5-4666-846a-1bd264829900︠









