︠096d0e99-0942-4eb1-aebb-133bbe7a4424︠
E4=EisensteinForms(1,4).basis()[0]
E6=EisensteinForms(1,6).basis()[0]
︡c8a11bac-7b17-47b1-9809-6fff692c93ac︡
︠5f1a36f2-2ffc-4a5c-8790-84f0bdd75bac︠
def as_poly_in_E4_E6(f):
    k = f.weight() # Das Gewicht von f
    s = f.parent().sturm_bound() # Dies ist die Anzahl der Koeffizienten, die man theoretisch maximal braucht. Wir benutzen das nachher zum Vergleich!
    fq = f.qexp(s)
    mon = [(a,(k-4*a)/6) for a in range(floor(k/4)+1) if (k-4*a) % 6 == 0] # Die zulaessigen Paare (a,b) fuer die Monome E4^a*E6^b
    M = len(mon)
    A = Matrix(M,M) # Diese Matrix wird mit den Koeff. der Monome initialisiert
    for i, (a,b) in enumerate(mon):
        g = E4.qexp(M)**a*E6.qexp(M)**b
        A.set_column(i, [g[j] for j in range(M)])
    #print A
    v=A.solve_right(vector([f[n] for n in range(M)])) # Der Loesungsvektor
    # Nun testen wir, ob das Ergebnis stimmt
    g = sum(v[i]*E4.qexp(s)**mon[i][0]*E6.qexp(s)**mon[i][1] for i in range(len(mon)))
    for n in range(s):
        assert g[n] == f[n]
    return v

︡509c33ca-36f4-4544-9922-3abd398fe0a4︡
︠44f4efeb-9f12-4c8b-a4ac-1604950c9c02︠
as_poly_in_E4_E6(CuspForms(1,30).basis()[1])
︡c9452251-6680-4deb-9f03-3d7e2f9a9939︡{"stdout":"(1/2985984, -1/1492992, 1/2985984)\n"}︡
︠42f3e3bf-50be-45bb-a1f1-7a24c50c519er︠
#Testen!
for k in range(1,200):
    for f in ModularForms(1,k).basis():
        h=as_poly_in_E4_E6(f)
︡4b92f0b1-0d8b-4f59-aa01-095d2a17ad0c︡
︠21600855-f8fd-461e-9258-e7a9e16c2694︠









