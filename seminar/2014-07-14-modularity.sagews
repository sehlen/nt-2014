︠47aa2a13-1bb1-4d7d-948f-f64b91731b59︠
E=EllipticCurve([0,-1,1,0,0])
E
︡8757959e-9c07-49c8-8240-c0e3d557a4fc︡{"stdout":"Elliptic Curve defined by y^2 + y = x^3 - x^2 over Rational Field\n"}︡
︠9edc8a44-6413-4534-b296-8d85201ae778︠
E.plot()
︡b2e4e955-5872-45b4-8223-861f687763a1︡{"once":false,"file":{"show":true,"uuid":"d5ad887b-254c-4fea-b4a7-686d527cf7a1","filename":"/projects/6931ad98-daf6-4573-a651-563956d3599c/.sage/temp/compute17dc0/17994/tmp_O4ZQGV.svg"}}︡
︠e33a44f8-ffe1-4dc2-b43c-16f023e90c19︠
E1=EllipticCurve([0,-1,0,0,1/4])
E1.plot()
︡41394a97-6a65-453f-8d27-578f907d0805︡{"once":false,"file":{"show":true,"uuid":"07454f04-f001-469d-a5c9-c710d6192380","filename":"/projects/6931ad98-daf6-4573-a651-563956d3599c/.sage/temp/compute17dc0/17994/tmp_DJRYs6.svg"}}︡
︠25d97ae0-0a89-4e67-b2f4-d8f9661c3384︠
E1.is_isomorphic(E)
︡ef0b169c-5845-4b6a-8fb0-5fa02a3c6da1︡{"stdout":"True\n"}︡
︠f3bd1463-cc3d-4f0a-90fe-8047f91a99c8︠
E.conductor()
︡823539db-3055-4390-b44f-9b93e5add9cc︡{"stdout":"11\n"}︡
︠11164922-d24b-4652-b0ef-6e24e04502aai︠
%md
$$ a_p = p + 1 - \#E(\mathbb{F}_p) $$
︡d3ec63d2-4bed-4ede-a586-98c86bb7b416︡{"md":"$$ a_p = p + 1 - \\#E(\\mathbb{F}_p) $$\n"}︡
︠76a10c9e-661a-4c07-a71d-db450f85348b︠
E.aplist(100)
︡916ab819-0f86-4d39-b63d-46d0dce81f95︡{"stdout":"[-2, -1, 1, -2, 1, 4, -2, 0, -1, 0, 7, 3, -8, -6, 8, -6, 5, 12, -7, -3, 4, -10, -6, 15, -7]\n"}︡
︠f7d716d9-265b-4433-bcf2-7886d6937095︠

︠9aa195ef-d2fa-4f1c-9972-0b4387f82caf︠
S=CuspForms(11,2)
S.dimension()
︡7227634e-90a8-4d7a-8baa-9835e897b44c︡{"stdout":"1\n"}︡
︠cfc06e60-55dc-400b-9970-877994eb0af3︠
f=S.basis()[0]; f.qexp(100)
︡4f408cf4-390e-4c0b-8faa-a21511f71394︡{"stdout":"q - 2*q^2 - q^3 + 2*q^4 + q^5 + 2*q^6 - 2*q^7 - 2*q^9 - 2*q^10 + q^11 - 2*q^12 + 4*q^13 + 4*q^14 - q^15 - 4*q^16 - 2*q^17 + 4*q^18 + 2*q^20 + 2*q^21 - 2*q^22 - q^23 - 4*q^25 - 8*q^26 + 5*q^27 - 4*q^28 + 2*q^30 + 7*q^31 + 8*q^32 - q^33 + 4*q^34 - 2*q^35 - 4*q^36 + 3*q^37 - 4*q^39 - 8*q^41 - 4*q^42 - 6*q^43 + 2*q^44 - 2*q^45 + 2*q^46 + 8*q^47 + 4*q^48 - 3*q^49 + 8*q^50 + 2*q^51 + 8*q^52 - 6*q^53 - 10*q^54 + q^55 + 5*q^59 - 2*q^60 + 12*q^61 - 14*q^62 + 4*q^63 - 8*q^64 + 4*q^65 + 2*q^66 - 7*q^67 - 4*q^68 + q^69 + 4*q^70 - 3*q^71 + 4*q^73 - 6*q^74 + 4*q^75 - 2*q^77 + 8*q^78 - 10*q^79 - 4*q^80 + q^81 + 16*q^82 - 6*q^83 + 4*q^84 - 2*q^85 + 12*q^86 + 15*q^89 + 4*q^90 - 8*q^91 - 2*q^92 - 7*q^93 - 16*q^94 - 8*q^96 - 7*q^97 + 6*q^98 - 2*q^99 + O(q^100)\n"}︡
︠2e69faf3-1d3d-402b-a3e4-1665dda40187i︠
%md
$$ f = q \prod_{n=1}^\infty (1-q^n)^2 \prod_{n=1}^\infty (1-q^{11n})^2 = \eta(\tau)^2\eta(11\tau)^2 $$
︡6c080829-882c-48e0-b903-9a4a15a96267︡{"md":"$$ f = q \\prod_{n=1}^\\infty (1-q^n)^2 \\prod_{n=1}^\\infty (1-q^{11n})^2 = \\eta(\\tau)^2\\eta(11\\tau)^2 $$\n"}︡
︠c643f8e2-ad26-4ca5-8b5d-02d8e1859211︠
M4=ModularForms(1,4); M4
︡6ee43316-48c0-411b-b24b-fa0e2cb51df0︡{"stdout":"Modular Forms space of dimension 1 for Modular Group SL(2,Z) of weight 4 over Rational Field\n"}︡
︠27add11d-57f8-4d06-ba3c-532f5b96cc4b︠
M4.basis()
︡2bb7bb70-39b0-494e-ad82-e4f620656b20︡{"stdout":"[\n1 + 240*q + 2160*q^2 + 6720*q^3 + 17520*q^4 + 30240*q^5 + O(q^6)\n]\n"}︡
︠fd35ff62-e6d0-42cf-b883-d2784c454d87︠
E4=M4.basis()[0]; E4
︡cdcd5ceb-ce4c-4a26-bdab-fb3ef930b834︡{"stdout":"1 + 240*q + 2160*q^2 + 6720*q^3 + 17520*q^4 + 30240*q^5 + O(q^6)\n"}︡
︠f8255b48-b23a-4a0f-b24d-8eae655460f6︠
E6=ModularForms(1,6).basis()[0]; E6
︡4663ae27-f50b-4c01-9211-e1466431a14f︡{"stdout":"1 - 504*q - 16632*q^2 - 122976*q^3 - 532728*q^4 - 1575504*q^5 + O(q^6)\n"}︡
︠61201251-9f9c-44fd-a70e-10e6140eb9b9︠
f=CuspForms(1,12).basis()[0]
︡a97f4aec-cbf5-477b-91f3-a90f8e25d257︡
︠7b9beff0-e083-44d7-90b5-05c2b677b479︠
(E4.qexp(10)^3-E6.qexp(10)^2)/1728-f.qexp(10)
︡517a22a7-bcd3-4131-ab35-feb25bb005ec︡{"stdout":"O(q^10)\n"}︡
︠c097fc9a-2b8e-44c0-9ea6-491464b0eabb︠
f
︡a57b30ec-aa98-43df-9170-d0a31eb86161︡{"stdout":"q - 24*q^2 + 252*q^3 - 1472*q^4 + 4830*q^5 + O(q^6)\n"}︡
︠7f31e8b4-178a-49b5-a886-8f1167de323f︠
A=Matrix(CC,2,2)
︡e5f9a5b7-fe44-45dc-9961-a94e180eed69︡
︠c23dc926-665c-4595-9bc3-47a96890ffa4︠
A[0,0]=pi
A[0,1]=pi
A[1,0]=5*pi
A[1,1]=6*pi
︡af8089f1-2648-4ef5-9b76-46f9f7531c7d︡
︠88d59560-cc74-47d9-a99f-9ced28757ea3︠
A
︡f25c8125-27da-4fea-83ec-c1e808fd3574︡{"stdout":"[3.14159265358979 3.14159265358979]\n[15.7079632679490 18.8495559215388]\n"}︡
︠81e8ae06-7a88-4870-857a-59fecb35e240︠
A.solve_right(vector([pi,2*pi]))
︡29047111-d7ce-48d9-836f-6a0c57a90b13︡{"stdout":"(4.00000000000000, -3.00000000000000)\n"}︡
︠291c8636-d7da-4d93-be21-fc6df09c9b5e︠









