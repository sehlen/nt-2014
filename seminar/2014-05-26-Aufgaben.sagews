︠5cc75413-e506-4413-8c0b-4801f17b7059i︠
%md
*** Aufgaben ***

1. Implementiere schnelles Exponenzieren modulo $n$. (Algorithm 2.3.11)
2. Implementiere den Miller-Rabin Primzahltest (Algorithm 2.4.4)
3. Implementiere den Lucas-Lehmer Primzahltest
4. Implementiere den Fermat-Primzahltest (kleiner Satz von Fermat, Theorem 2.4.1)
5. Teste, Deine Implementationen immer auf Korrektheit
6. Fertige Statistiken an, wie gut die Tests sind.

Zum Beispiel kann man Fragen untersuchen, wie:

* Wie viele Zahlen kleiner einer gegebenen Schranke sind Pseudoprimzahlen bzgl einer gegebenen Basis, aber keine Primzahlen?
* Wie viele Basen (oder welche Basen -> Beispiel) brauche ich, um keine falschen Positiven zu bekommmen (kleiner einer Schranke)?
* etc.
︡39c09268-f0cd-4eab-aa93-1ea2a16174b8︡{"html":"<p><strong>* Aufgaben *</strong></p>\n\n<ol>\n<li>Implementiere schnelles Exponenzieren modulo $n$. (Algorithm 2.3.11)</li>\n<li>Implementiere den Miller-Rabin Primzahltest (Algorithm 2.4.4)</li>\n<li>Implementiere den Lucas-Lehmer Primzahltest</li>\n<li>Implementiere den Fermat-Primzahltest (kleiner Satz von Fermat, Theorem 2.4.1)</li>\n<li>Teste, Deine Implementationen immer auf Korrektheit</li>\n<li>Fertige Statistiken an, wie gut die Tests sind.</li>\n</ol>\n\n<p>Zum Beispiel kann man Fragen untersuchen, wie:</p>\n\n<ul>\n<li>Wie viele Zahlen kleiner einer gegebenen Schranke sind Pseudoprimzahlen bzgl einer gegebenen Basis, aber keine Primzahlen?</li>\n<li>Wie viele Basen (oder welche Basen -> Beispiel) brauche ich, um keine falschen Positiven zu bekommmen (kleiner einer Schranke)?</li>\n<li>etc.</li>\n</ul>\n"}︡
︠5ab53a41-1bf9-44b8-8f65-822fc2fe61b9︠









