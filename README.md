# Zahlentheorie: eine praktische Einführung
- Seminar im Sommersemester 2014
- Technische Universität Darmstadt

## Kontakt
- Dr. Stephan Ehlen
- <ehlen@mathematik.tu-darmstadt.de>
- Homepage: <http://www.mathematik.tu-darmstadt.de/~ehlen>
- Büro: S 215 | 441
- Sprechstunde: nach Vereinbarung (am Besten Email schreiben)

## Ort und Zeit
- Das Seminar wird in 4 Blöcken veranstaltet
- Die Termine sind: 5.5., 26.5., 16.6., 14.7
- Ort: S2|15 401
- Beginn: 11:40 Uhr

## Inhalte des Seminars
- Primzahlen
- Die Riemannsche Vermutung
- Public-Key Kryptographie
- Elliptische Kurven
- Elliptische Kurven und Modulformen, Fermats letzter Satz
- Die Vermutung von Birch und Swinnerton-Dyer
- Arbeiten mit `sage` und der SageMathCloud

## SageMathCloud
1. Melde Dich unter [http://cloud.sagemath.com](http://cloud.sagemath.com) an
2. Erstelle ein Projekt für das Seminar
3. Füge mich (Stephan Ehlen, <stephan.j.ehlen@gmail.com>) als *collaborator* hinzu.
So habe ich Zugang zu den von Dir erstellten Dateien,
kann jederzeit weiterhelfen und die Arbeit am Ende bewerten.

## Auf die Seminardateien zugreifen
2. In Deinem Projekt in SMC zu diesem Seminar, klick auf den Button "+New"
3. Gib die URL dieses *git repository* in die Eingabezeile ein:
>   https://sehlen@bitbucket.org/sehlen/nt-2014.git

4. Klick auf "Copy from Internet Link"
5. Um die aktuelle Version zu erhalten, musst Du in einem Terminal in dem Verzeichnis `nt-2014` (oder wie Du es ggf. umbenannt hast)
```
   git pull
```
eingeben.

## Aktuelles:
- Am Mittwoch, 16.04.2014:
    16:15-16:45 Uhr, Raum S2|15 234
    ["What is...?" Modulformen](http://www3.mathematik.tu-darmstadt.de/fb/mathe/veranstaltungen/what-is.html)
- Danach Tee in Raum 244
- Und dann (17:15)
    Antrittsvorlesung von Prof. Dr. Anna von Pippich:
    "Summen natürlicher Zahlen . . . unendlich einfach oder einfach unendlich?"
    S2|14 24 (Höorsaal der Physik gegenüber vom Mathebau)
- Hinweis: Das Seminar [Modulformen](http://www3.mathematik.tu-darmstadt.de/ags/algebra/lehre/seminare/seminar-modulformen-ss-14.html)
  bei Herrn Prof. Bruinier kann auch durchaus parallel besucht werden.
- Außerdem gibt es noch ein [Seminar zu Elliptischen Kurven](http://www3.mathematik.tu-darmstadt.de/fileadmin/home/groups/2/habegger_ell.pdf) bei Herrn Prof. Habegger.

## Vorträge
### 5.5.2014
#### Primzahlen (Pia Potrikus)

Inhalt:

- Fundamentalsatz der Arithmetik
- ggT
- Sieb des Eratosthenes
- Primzahlen in arithmetischen Progressionen (kurz)
- Der Primzahlsatz (Tschebyscheff: Richtig mit max. 11% Fehler, Erdös/Selberg: "elementarer" Beweis (max. Skizze der Zutaten))

Literaturhinweise:

- [ENT][ent], Kapitel 1
- [MSP][msp], S. 1-18
- [Zag][zag]
- [ES][es]
- [rho][rho] (Elementarer Beweis des Primzahlsatzes, nach Hardy & Wright, Erdös, Selberg)


#### Die Riemannsche Vermutung (Susanne Rieß)

Inhalt:

- Die Riemannsche Zetafunktion
- Verschiedene Formulierungen der Riemannschen Vermutung (RH für *Riemann Hypothesis* im Englischen)
- Evidenz, die für die Vermutung spricht
- Folgerungen aus der RH (Auszug)
- Li(x) und Riemann's explizite Formel für pi(x)
- (Verallgemeinerungen von RH)

Literaturhinweise:

- [ENT][ent], Kapitel 1
- [RH][rh], Teil I
- [Zag][zag]
- [Neu][neu], Kapitel VII, Paragraph 1
- [LMFDB][lmfdb]

### 26.5.2014

#### Z/nZ und Primzahltests 1 (Albert Roth)

Inhalt:

- Lineare Kongruenzen, Chinesischer Restsatz
- (Quadratisches Restsymbol)
- Satz von Euler, Eulersche Phi-Funktion
- Satz von Wilson
- Pseudoprimzahlen, Carmichael-Zahlen
- Miller-Rabin Primzahltest

Literaturhinweise:

- [ENT][ent], Kapitel 2
- [PN][pn], Kapitel 3 (Theorem 3.5.6: Miller-Rabin)
- [Terry Tao zum Lucas-Lehmer Primzahltest][taol]


#### Primzahltests 2 (Chenfei Zhou)

Inhalt:

- Lucas-Lehmer Primzahltest

Literaturhinweise:

- [ENT][ent], Kapitel 2
- [PN][pn], Kapitel 4 (Theorem 4.2.6: Lucas-Lehmer, Abschnitt 4.5: AKS)
- [Terry Tao zum Lucas-Lehmer Primzahltest][taol]


### 16.6.2014

#### Elliptische Kurven (Sabrina Pauli)

Inhalte:

- Einführung in Elliptische Kurven
- Die Gruppenstruktur
- Elliptische Kurven über den komplexen Zahlen, rationalen Zahlen und über endlichen Körpern

Literaturhinweise:

- [ENT][ent], Kapitel 6 (6.1, 6.2)
- [Kob][kob]

#### Faktorisierungsalgorithmen (Ellen Carmesin)

Inhalte:

- Pollard's (p-1)-Methode
- Lenstra's Faktorisierungsalgorithmus

Literaturhinweise:

- [ENT][ent], Kapitel 6 (6.3)
- [Kob][kob]
- [Kob3][kob3]

#### Kryptographie (mit ellitpischen Kurven) (Johannes Werner)

Inhalte (ggf. Auswahl treffen):

- Diffie-Hellman
- RSA
- ECDSA
- ElGamal (Verschlüsselung)
- Der Fehler in der Implementation von ECDSA in der PS3 von Sony
- Bitcoin

Literaturhinweise:

- [ENT][ent], Kapitel 6 (6.4)
- ecdsa1.sagews (von William Stein, in diesem Repository)
- ecdsa2.sagews (---"---)
- [Kob2][kob2]
- [Kob3][kob3]

### 14.07.2014

#### Elliptische Kurven und Modulformen (Vanessa Grimm)


## Literatur, Linksammlung (wird noch erweitert)
### Mathematisch

- Crandall, Pomerance, [Prime numbers, A computational perspective][pn], Second Edition, Springer Verlag, 2005, [ULB](http://dakapo.ulb.tu-darmstadt.de/servlet/bdvlocator;jsessionid=0A71924CC67F489A35ADD6B7BAE9CA0B?_position=0&isbn=0-387-25282-7&aut2=Pomerance%2C+Carl+&mat=book&aut1=Crandall%2C+Richard+E.+&picaid=190220090&dc-type=Aau&title=Prime+numbers&qdate=2005&date=2005&sad=ROOT%3AULB-DA%3AModul-DA-TU%40LBS-DA-LHB%244&sid=OPC4%3ALBS-DA-LHB)
- Dorian Goldfeld, [The Erdös-Selberg dispute][es]
- Neil Koblitz, [Introduction to elliptic curves and modular forms][kob]
- Neil Koblitz, [Elliptic curve cryptosystems][kob2]
- Neil Koblitz, [A Course in Number Theory and Cryptography][kob3]
- Barry Mazur, William Stein, [PRIMES][rh]
- S. Müller-Stach, J. Piontkowski, [Elementare und algebraische Zahlentheorie][msp], Vieweg-Verlag
- J. Neukirch, [Algebraische Zahlentheorie][neu]
- William Stein, [Elementary Number Theory: Primes, Congruences, and Secrets][ent], Springer Verlag, 2008
- Terry Tao [The Lucas-Lehmer test for Mersenne primes][taol] (Auch gut als Einführung)
- Don Zagier, [Die ersten 50 Millionen Primzahlen][zag]
- LMFDB: [The L-Functions and Modular forms database][lmfdb]

[pn]: http://www.springer.com/mathematics/numbers/book/978-0-387-25282-7
[es]: http://www.math.columbia.edu/~goldfeld/ErdosSelbergDispute.pdf
[ent]: http://wstein.org/ent/
[msp]: http://link.springer.com/book/10.1007%2F978-3-8348-9064-1
[neu]: http://pica1l.lhb.tu-darmstadt.de/CHARSET=iso-8859-1/DB=LHBDA/CMD?SRT=YOP&ACT=SRCHA&IKT=12&SRT=YOP&TRM=017700922
[rh]: http://wstein.org/rh/
[zag]: https://www.uni-due.de/~hm0019/lehre-ws1011/pdf/zagier.pdf
[rho]: http://rho.math.uni-rostock.de/SemSkripte/Primzahlsatz.pdf
[taol]: http://terrytao.wordpress.com/2008/10/02/the-lucas-lehmer-test-for-mersenne-primes/
[lmfdb]: http://www.lmfdb.org
[kob]: http://link.springer.com/book/10.1007%2F978-1-4612-0909-6
[kob2]: http://www.ams.org/journals/mcom/1987-48-177/S0025-5718-1987-0866109-5/
[kob3]: http://www.springer.com/mathematics/numbers/book/978-0-387-94293-3


### Sage und Cloud
- [SageMathCloud Help](https://cloud.sagemath.com/help)
- [SageMathCloud FAQ](https://github.com/sagemath/cloud/wiki/FAQ)
- [Sage Tutorial](http://www.sagemath.org/doc/tutorial/index.html)
- [Sage-Kurs von W. Stein (aktuell)](https://github.com/williamstein/sage2014)

### Python
- [Python Tutorial](https://docs.python.org/2/tutorial/)
- [Python Documentation](https://docs.python.org/2/)



